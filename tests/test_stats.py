import os

from corcua import stats
from corcua.readers.base_reader import Reader as BaseReader

# ==================================================================================================


def test_get_duration_default() -> None:
    # Load our test dataset
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    datapath = file_path + "data/base_dataset/"
    ds = BaseReader().load_dataset({"path": datapath})

    ds = stats.get_duration(ds)
    assert ds[0]["duration"] == 4.572


# ==================================================================================================


def test_get_duration_segments() -> None:
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    ds = [
        {
            "filepath": file_path + "data/base_dataset/test.wav",
            "segments": [
                {"text": "A", "time_begin": 0.0, "time_end": 1.4},
                {"text": "B", "time_begin": 1.5, "time_end": 3.1},
            ],
        }
    ]

    ds = stats.get_duration(ds)
    assert ds[0]["duration"] == 3.0
