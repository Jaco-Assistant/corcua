import os

from corcua import utils

# ==================================================================================================


def test_seconds_to_hours() -> None:
    assert utils.seconds_to_hours(123.45) == "0:02:03"
    assert utils.seconds_to_hours(3636.63) == "1:00:36"


# ==================================================================================================


def test_duration() -> None:
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    wavpath = file_path + "data/base_dataset/test.wav"

    assert utils.get_duration(wavpath) == 4.572


# ==================================================================================================


def test_partitions_1() -> None:
    ds = [
        {"filepath": "a.wav", "duration": 4, "text": "a"},
        {"filepath": "b.wav", "duration": 2, "text": "b"},
        {"filepath": "c.wav", "duration": 1, "text": "c"},
        {"filepath": "d.wav", "duration": 1, "text": "d"},
        {"filepath": "e.wav", "duration": 3, "text": "e"},
        {"filepath": "f.wav", "duration": 1, "text": "f"},
        {"filepath": "g.wav", "duration": 2, "text": "g"},
        {"filepath": "h.wav", "duration": 3, "text": "h"},
        {"filepath": "i.wav", "duration": 2, "text": "i"},
        {"filepath": "j.wav", "duration": 1, "text": "j"},
    ]
    splits = [("train", 0.8), ("val", 0.1), ("test", 0.1)]
    dall = sum((e["duration"] for e in ds))  # type: ignore[misc]
    assert dall == 20

    ds = utils.add_partitions(ds, splits, group_key=None)
    assert set(utils.get_partitions(ds)) == {"train", "val", "test"}

    dtrain = sum((e["duration"] for e in ds if e["partition"] == "train"))  # type: ignore[misc]
    dval = sum((e["duration"] for e in ds if e["partition"] == "val"))  # type: ignore[misc]
    dtest = sum((e["duration"] for e in ds if e["partition"] == "test"))  # type: ignore[misc]

    # The split is not evenly, because the second and third elements are bigger than the 10% share
    # of the val and test partitions
    assert dtrain == 14 and dval == 3 and dtest == 3


# ==================================================================================================


def test_partitions_2() -> None:
    ds = [
        {"filepath": "a.wav", "duration": 4, "text": "a", "speaker": "u"},
        {"filepath": "b.wav", "duration": 2, "text": "b", "speaker": "v"},
        {"filepath": "c.wav", "duration": 1, "text": "c", "speaker": "w"},
        {"filepath": "d.wav", "duration": 1, "text": "d", "speaker": "x"},
        {"filepath": "e.wav", "duration": 3, "text": "e", "speaker": "y"},
        {"filepath": "f.wav", "duration": 1, "text": "f", "speaker": "u"},
        {"filepath": "g.wav", "duration": 2, "text": "g", "speaker": "v"},
        {"filepath": "h.wav", "duration": 3, "text": "h", "speaker": "w"},
        {"filepath": "i.wav", "duration": 2, "text": "i", "speaker": "x"},
        {"filepath": "j.wav", "duration": 1, "text": "j", "speaker": "y"},
    ]
    splits = [("train", 0.8), ("val", 0.1), ("test", 0.1)]
    dall = sum((e["duration"] for e in ds))  # type: ignore[misc]
    assert dall == 20

    ds = utils.add_partitions(ds, splits, group_key="speaker")
    assert set(utils.get_partitions(ds)) == {"train", "val", "test"}

    dtrain = sum((e["duration"] for e in ds if e["partition"] == "train"))  # type: ignore[misc]
    dval = sum((e["duration"] for e in ds if e["partition"] == "val"))  # type: ignore[misc]
    dtest = sum((e["duration"] for e in ds if e["partition"] == "test"))  # type: ignore[misc]

    # The split is not evenly, because the second and third elements are bigger than the 10% share
    # of the val and test partitions
    assert dtrain == 12 and dval == 4 and dtest == 4


# ==================================================================================================


def test_pathsavestring() -> None:
    tstr = "/path/to/strange &%$?§%audio.wav"
    uts = utils.PathSaveString()

    estr = uts.encode(tstr)
    dstr = uts.decode(estr)

    assert tstr == dstr
    assert estr.isalnum()

    tstr = (
        "very long string 2F646174615F6F726967696E616C2F64652F6D61696C6162732F16"
        "2795F626F6F6B2F62795F626F6F6B2F66656D616C6A52F726562656363615F627261756"
        "E6572745F706C756E6B6574742F6D65696E5FB7765675F616C735F64657574736368657"
        "25F756E645F6A7564652F776176732F6D65696E5F7765675F616C735F64657574736363"
    )

    estr = uts.encode(tstr)
    dstr = uts.decode(estr)

    assert tstr == dstr
    assert estr.isalnum()
