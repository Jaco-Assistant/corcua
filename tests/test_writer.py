import os
import shutil

from corcua import writers  # pylint: disable=unused-import
from corcua.readers.base_reader import Reader as BaseReader
from corcua.writers.base_writer import Writer as BaseWriter

# ==================================================================================================


def test_writing_default() -> None:
    # Load our test dataset
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    datapath = file_path + "data/base_dataset/"
    ds = BaseReader().load_dataset({"path": datapath})

    # Add an additional entry to test partition splitting
    ds.append(dict(ds[0]))
    ds[0]["partition"] = "train"
    ds[1]["partition"] = "test"

    # Test writing
    path = "/tmp/coruca/testw/"
    BaseWriter().save_dataset(ds, path=path, sample_rate=16000, overwrite=True)
    assert os.path.exists("/tmp/coruca/testw/all.csv")
    assert os.path.exists("/tmp/coruca/testw/train.csv")
    assert os.path.exists("/tmp/coruca/testw/test.csv")

    # Make sure that we can read it again
    ds = BaseReader().load_dataset({"path": path})
    assert len(ds) == 2

    # Delete created files
    shutil.rmtree("/tmp/coruca/testw/", ignore_errors=True)


# ==================================================================================================


def test_writing_segments() -> None:
    # Create example dataset
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
    ds = [
        {
            "filepath": file_path + "data/base_dataset/test.wav",
            "segments": [
                {"text": "A", "time_begin": 0.0, "time_end": 1.4},
                {"text": "B", "time_begin": 1.5, "time_end": 3.1},
            ],
        }
    ]

    # Test writing
    path = "/tmp/coruca/testw/"
    _ = BaseWriter().save_dataset(ds, path=path, sample_rate=16000, overwrite=True)
    assert os.path.exists("/tmp/coruca/testw/all.csv")
    assert len(os.listdir(path + "audios/")) == 2

    # Make sure that we can read it again
    dsB = BaseReader().load_dataset({"path": path})
    assert len(dsB) == 2
    assert dsB[0]["text"] == "A"

    # Delete created files
    shutil.rmtree("/tmp/coruca/testw/", ignore_errors=True)
