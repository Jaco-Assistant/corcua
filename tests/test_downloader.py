import os
import shutil

from corcua import downloaders  # pylint: disable=unused-import
from corcua.downloaders.abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        link = "https://gitlab.com/Jaco-Assistant/corcua/-/raw/master/tests/data/download_files/"
        self.targz_link = link + "test.tar.gz?inline=false"
        self.zip_link = link + "test.zip?inline=false"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        super().download_dataset(path, overwrite, args)

        AbstractDownloader.download_and_extract_targz(self.targz_link, path + "targz/")
        AbstractDownloader.download_and_extract_zip(self.zip_link, path + "zip/", False)


# ==================================================================================================


def test_downloading() -> None:
    Downloader().download_dataset(path="/tmp/coruca/testdl/", overwrite=True, args={})

    assert os.path.exists("/tmp/coruca/testdl/targz/test.txt")
    assert os.path.exists("/tmp/coruca/testdl/zip/test.txt")
    shutil.rmtree("/tmp/coruca/testdl/", ignore_errors=True)
