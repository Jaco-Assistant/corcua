# Corcua

Corcua is a library to load audio datasets and export them to different framework formats.
The goal in creating it, was to make adding new datasets as easy as possible.

[![pipeline status](https://gitlab.com/Jaco-Assistant/corcua/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/corcua/-/commits/master)
[![coverage report](https://gitlab.com/Jaco-Assistant/corcua/badges/master/coverage.svg)](https://gitlab.com/Jaco-Assistant/corcua/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/corcua/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/corcua/-/commits/master)

<br>

## Supported Datasets

| Name                                                                                                                              | Language                                                                       | Length (hours)                                     | Additional Keys                                                                                               | Additional Infos                                                                                                                                                                                                                                                                                                                                                                               |
| --------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------ | -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Alcohol Language Corpus](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/ALC/ALC.4.php)                     | de                                                                             | 48                                                 | age, blood_alcohol, breath_alcohol, day_emotion, drinking_habit, gender, school_region, test_emotion, weather | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [BAS-FormTask](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/FORMTASK/FORMTASK.2.php)                      | de                                                                             | 18                                                 | speaker                                                                                                       | Download it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/FORMTASK/FORMTASK.2.php).                                                                                                                                                                                                                                                                              |
| [BAS-SprecherInnen](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/SprecherInnen/SprecherInnen.1.php)       | de                                                                             | 2                                                  | speaker                                                                                                       | Download it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/index.php?target=Public/Corpora/SprecherInnen/SprecherInnen.1.php).                                                                                                                                                                                                                                                   |
| [BirdCLEF-2021](https://www.kaggle.com/c/birdclef-2021/overview)                                                                  |                                                                                | 989                                                | birds                                                                                                         | Import and convert it first, afterwards split long audio files with: `python3 corcua/extras/birdclef-splitting.py --path_source "../data_prepared/xx/birdclef-tmp/" --path_target "../data_prepared/xx/birdclef/"`. Preparation takes about 3h in total.                                                                                                                                       |
| [Brothers](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/BROTHERS/BROTHERS.2.php)                          | de                                                                             | 7                                                  | brother, family, session                                                                                      | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [CommonVoice](https://commonvoice.mozilla.org/)                                                                                   | de <br> en <br> es <br> fr <br> it <br> ?                                      | 1,093 <br> 2,224 <br> 407 <br> 848 <br> 315 <br> ? | accent, age, client_id, down_votes, gender, locale, segment, up_votes                                         | Download from [link](https://commonvoice.mozilla.org/en/datasets). The training partition contains all audios from the validated.tsv file, except those which are already in the dev.tsv and test.tsv files. Downloading the English v9 dataset takes about 6h, conversion about 13h.                                                                                                          |
| [CSS-10](https://github.com/Kyubyong/css10)                                                                                       | de <br> es <br> fr <br> ?                                                      | 16 <br> 24 <br> 19 <br> ?                          | speaker, text-raw                                                                                             | Get it [here](https://github.com/Kyubyong/css10), requires free Kaggle account.                                                                                                                                                                                                                                                                                                                |
| [Elex](https://www.gog.com/game/elex)                                                                                             | de <br> en, pl                                                                 | 26 <br> ?                                          | speaker                                                                                                       | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [GigaSpeech](https://github.com/SpeechColab/GigaSpeech)                                                                           | en                                                                             | 9363                                               | category, source                                                                                              | Follow the readme of the dataset's repository for downloading this dataset, note that you can select the different subsets with an extra flag. Note that this dataset is huge and requires about 1.2TB for downloading, twice the amount temporarily for conversion and 500GB for the converted files and about 120GB memory. Download takes about 71h, preparation about 53h with 20 threads. |
| [Gothic1](https://www.gog.com/game/gothic)                                                                                        | de <br> en, pl                                                                 | 7 <br> ?                                           |                                                                                                               | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [Guild2-Renaissance](https://www.gog.com/game/the_guild_2_renaissance)                                                            | de <br> en, es, fr, it                                                         | 11 <br> ?                                          | emotion, speaker                                                                                              | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [Hempel](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/HEMPEL/HEMPEL.4.php)                                | de                                                                             | 25                                                 | accent, age, gender, speaker                                                                                  | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [KingdomCome-Deliverance](https://www.gog.com/de/game/kingdom_come_deliverance_royal_edition)                                     | de <br> cs, en, fr, ja                                                         | 119 <br> ?                                         | speaker                                                                                                       | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [LibriSpeech](http://www.openslr.org/12)                                                                                          | en                                                                             | 982                                                | chapter, gender, speaker                                                                                      |                                                                                                                                                                                                                                                                                                                                                                                                |
| [LibriVox-Spanish](https://www.kaggle.com/carlfm01/120h-spanish-speech)                                                           | es                                                                             | 120                                                |                                                                                                               | Get it [here](https://www.kaggle.com/carlfm01/120h-spanish-speech), requires free Kaggle account. All files are duplicated, you can delete one of the folders. Load with `deepspeech` reader.                                                                                                                                                                                                  |
| [LinguaLibre](https://lingualibre.org/wiki/LinguaLibre:Main_Page)                                                                 | de <br> es <br> fr <br> it <br> ?                                              | 4 <br> 1 <br> 45 <br> 1 <br> ?                     | speaker                                                                                                       |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Mailabs](https://www.caito.de/2019/01/the-m-ailabs-speech-dataset/)                                                              | de <br> es <br> fr <br> it <br> en, pl, ru, uk                                 | 234 <br> 109 <br> 184 <br> 128 <br> ?              | book, gender, speaker                                                                                         |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Multilingual LibriSpeech](http://www.openslr.org/94/)                                                                            | de <br> en, es, fr, it, nl, pl, pt                                             | 1995 <br> ?                                        | book, speaker                                                                                                 |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Multilingual TEDx](http://www.openslr.org/100/)                                                                                  | de <br> es <br> fr <br> it <br> ar, el, pt, ru                                 | 14 <br> 185 <br> 183 <br> 106 <br> ?               | speaker                                                                                                       |                                                                                                                                                                                                                                                                                                                                                                                                |
| [PhattSessionz](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/PHATTSESSIONZ/PHATTSESSIONZ.2.php)           | de                                                                             | 238                                                |                                                                                                               | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. Download as EmuDB. The dataset might contain more additional keys.                                                                                                                                                                                                           |
| [PhoneDat 1](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/PD1/PD1.3.php)                                  | de                                                                             | 21                                                 | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Regional Variants of German](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/RVG1_CLARIN/RVG1_CLARIN.3.php) | de                                                                             | 129                                                | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [RVG - Juveniles](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/RVG-J/RVG-J.2.php)                         | de                                                                             | 49                                                 | accent, age, gender, speaker                                                                                  | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [SC10](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/SC10/SC10.4.php)                                      | de                                                                             | 6                                                  | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Smartweb Handheld Corpus](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/SHC/SHC.2.php)                    | de                                                                             | 29                                                 |                                                                                                               | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. Download as EmuDB. The dataset might contain more additional keys.                                                                                                                                                                                                           |
| [SI100](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/SI100/SI100.2.php)                                   | de                                                                             | 36                                                 | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Smartweb Motorbike Corpus](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/SMC/SMC.2.php)                   | de                                                                             | 6                                                  | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Skyrim Legacy+DLCs](https://store.steampowered.com/app/72850/The_Elder_Scrolls_V_Skyrim/)                                        | de <br> ?                                                                      | 89 <br> ?                                          | emotion, gamescene, speaker                                                                                   | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [Spoken Wikipedia Corpus](https://nats.gitlab.io/swc/)                                                                            | de <br> en, nl                                                                 | 258 <br> ?                                         | dialect, gender, speaker                                                                                      |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Tatoeba](https://tatoeba.org/)                                                                                                   | de <br> es <br> fr <br> ?                                                      | 8 <br> 60 <br> 2 <br> ?                            |                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Thorsten](http://www.openslr.org/95/)                                                                                            | de                                                                             | 23                                                 |                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                |
| [TrainingSpeech-FR](https://github.com/wasertech/TrainingSpeech)                                                                  | fr                                                                             | 183                                                |                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Tuda](https://www.inf.uni-hamburg.de/en/inst/ab/lt/resources/data/acoustic-models.html)                                          | de                                                                             | 185                                                | ageclass, gender, is_native, microphone, region, speaker                                                      |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Verbmobil 1](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/VM1/VM1.3.php)                                 | de <br> en, ja                                                                 | 34 <br> ?                                          | dialog + turn, recording_site, scenario, speaker                                                              | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Verbmobil 2](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/VM2/VM2.3.php)                                 | de <br> en, ja                                                                 | 22 <br> ?                                          | dialog + turn, recording_device, recording_mode, scenario, speaker                                            | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Voxforge](http://www.voxforge.org/home)                                                                                          | de <br> es <br> fr <br> it <br> ?                                              | 33 <br> 52 <br> 37 <br> 20 <br> ?                  | age, dialect, gender, microphone, speaker                                                                     |                                                                                                                                                                                                                                                                                                                                                                                                |
| [Voxpopuli](https://github.com/facebookresearch/voxpopuli)                                                                        | de <br> en <br> es <br> fr <br> it, pl, ro, hu, cs, nl, fi, hr, sk, sl, et, lt | 283 <br> 543 <br> 166 <br> 211 <br> ?              | speaker                                                                                                       | Download takes up about 73GB, similar amount is required temporarily for conversion. Converted dataset will need about half that size, depending on the language. Preparation is comparatively slow and takes about 11h.                                                                                                                                                                       |
| [WaSeP](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/WaSeP/WaSeP.2.php)                                   | de                                                                             | 3                                                  |                                                                                                               | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [Witcher3-GOTY](https://www.gog.com/game/the_witcher_3_wild_hunt_game_of_the_year_edition)                                        | de <br> en, fr, ja, pl, pt, ru                                                 | 44 <br> ?                                          | speaker, listener                                                                                             | See [Games](#games) section.                                                                                                                                                                                                                                                                                                                                                                   |
| [YouTube](https://www.youtube.com/)                                                                                               | all                                                                            |                                                    |                                                                                                               | Download YouTube playlists with handmade subtitles. Check some videos before, to ensure the transcriptions are well aligned.                                                                                                                                                                                                                                                                   |
| [Zamia-Speech](https://goofy.zamia.org/zamia-speech/corpora/)                                                                     | de                                                                             | 19                                                 | speaker                                                                                                       |                                                                                                                                                                                                                                                                                                                                                                                                |
| [ZipTel](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/ZIPTEL/ZIPTEL.3.php)                                | de                                                                             | 13                                                 | gender, speaker                                                                                               | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. The dataset might contain more additional keys.                                                                                                                                                                                                                              |
| [ZTC-BAS](https://clarin.phonetik.uni-muenchen.de/BASRepository/Public/Corpora/ZTC_BAS/ZTC_BAS.2.php)                             | de                                                                             | 84                                                 | speaker                                                                                                       | Get it [here](https://clarin.phonetik.uni-muenchen.de/BASRepository/). Requires login with an university account. Contains only very long audio files. The dataset might contain more additional keys.                                                                                                                                                                                         |

<br>

## Installation

Install ffmpeg (required for audio format conversions):

```bash
sudo apt-get install -y ffmpeg
```

Install corcua:

```bash
git clone https://gitlab.com/Jaco-Assistant/corcua.git
pip3 install --user -e corcua/
```

<br>

## Usage examples

```python
# Downloading
from corcua import downloaders
downloaders.librispeech.Downloader().download_dataset(path="../data_original/en/librispeech/", overwrite=True, args={"testonly": True})
downloaders.lingualibre.Downloader().download_dataset(path="../data_original/de/lingualibre/", overwrite=True, args={"language": "deu"})
downloaders.mailabs.Downloader().download_dataset(path="../data_original/de/mailabs/", overwrite=True, args={"language": "de_DE"})
downloaders.mls.Downloader().download_dataset(path="../data_original/de/MLS/", overwrite=True, args={"language": "de"})
downloaders.mtedx.Downloader().download_dataset(path="../data_original/de/mTEDx/", overwrite=True, args={"language": "de-de"})
downloaders.swc.Downloader().download_dataset(path="../data_original/de/SWC/", overwrite=True, args={"language": "de"})
downloaders.tatoeba.Downloader().download_dataset(path="../data_original/de/tatoeba/", overwrite=True, args={"language": "deu", "license_filter":""})
downloaders.thorsten.Downloader().download_dataset(path="../data_original/de/thorsten/", overwrite=True, args={})
downloaders.train_speech_fr.Downloader().download_dataset(path="../data_original/fr/train_speech_fr/", overwrite=True, args={})
downloaders.tuda.Downloader().download_dataset(path="../data_original/de/tuda/", overwrite=True, args={})
downloaders.voxforge.Downloader().download_dataset(path="../data_original/de/voxforge/", overwrite=True, args={"language": "de"})
downloaders.voxpopuli.Downloader().download_dataset(path="../data_original/de/voxpopuli/", overwrite=True, args={"language": "de"})
downloaders.youtube.Downloader().download_dataset(path="../testdl/", overwrite=True, args={"link": "https://www.youtube.com/watch?v=CSwivAbOhio&list=UU9RSWjfMU3qMixhigyHjEgw", "lang": "de"})
downloaders.zamia_speech.Downloader().download_dataset(path="../data_original/de/zamia_speech/", overwrite=True, args={})

# Read datasets
from corcua import readers
ds = readers.alc.Reader().load_dataset({"path": "/media/isse/DATA/data_original/all.ALC.4.cmdi.15660.1606394935/ALC/"})
ds = readers.bas_sprecherinnen.Reader().load_dataset({"path": "../DeepSpeech-German/data_original/de/SprecherInnen/"})
ds = readers.bas_formtask.Reader().load_dataset({"path": "../DeepSpeech-German/data_original/de/FORMTASK/"})
ds = readers.bethesda.Reader().load_dataset({"path_wavs": "C:\\Users\\Daniel\\Downloads\\LazyVoiceFinder v1.3.6-82482-1-3-6\\LazyVoiceFinder\\ExportWav", "path_csv": "C:\\Users\\Daniel\\Downloads\\LazyVoiceFinder v1.3.6-82482-1-3-6\\LazyVoiceFinder\\LazyVoiceFinder_export.csv", "mode": "skyrim"})
ds = readers.birdclef.Reader().load_dataset({"path": "../data_original/xx/birdclef-2021/"})
ds = readers.brothers.Reader().load_dataset({"path": "../data_original/de/BROTHERS/"})
ds = readers.common_voice.Reader().load_dataset({"path": "data_original/de/common_voice/de/"})
ds = readers.css_ten.Reader().load_dataset({"path": "../data_original/de/css_ten/"})
ds = readers.elex.Reader().load_dataset({"speechpath": "E:\\ELEX\\data\\packed\\c_1_de\\Dialogue_de", "stringpath": "E:\\ELEX\\data\\raw\\strings\\strings.csv", "language": "de"})
ds = readers.gigaspeech.Reader().load_dataset({"path": "../data_original/en/gigaspeech/"})
ds = readers.gothic1.Reader().load_dataset({"speechpath": "C:\\Users\\Daniel\\Downloads\\GothicVDFS\\_DevKit\\Tools\\VDFS\\_WORK\\DATA\\SOUND\\SPEECH", "storypath": "C:\\Users\\Daniel\\Downloads\\gothic_mod_developmentkit\\GOTHIC MOD Development Kit\\gothic\\_work\\data\\Scripts\\content\\Story"})
ds = readers.guild2r.Reader().load_dataset({"path": "E:\\The Guild 2 - Renaissance"})
ds = readers.hempel.Reader().load_dataset({"path": "../data_original/de/HEMPEL/"})
ds = readers.kingdomcome.Reader().load_dataset({"path": "/mnt/7C6E1A886E1A3AFA/Kingdom Come Deliverance/", "language": "german"})
ds = readers.librispeech.Reader().load_dataset({"path": "../data_original/en/librispeech/"})
ds = readers.lingualibre.Reader().load_dataset({"path": "../data_original/de/lingualibre/"})
ds = readers.mailabs.Reader().load_dataset({"path": "../data_original/de/mailabs/"})
ds = readers.mtedx.Reader().load_dataset({"path": "../data_original/de/mTEDx/de-de/data/", "language": "de"})
ds = readers.mls.Reader().load_dataset({"path": "../data_original/de/MLS/mls_german_opus/"})
ds = readers.phattsessionz.Reader().load_dataset({"path": "../data_original/de/PHATTSESSIONZ_emuDB/"})
ds = readers.phonedat1.Reader().load_dataset({"path": "../data_original/de/PD1/"})
ds = readers.rvg1.Reader().load_dataset({"path": "/media/isse/DATA/data_original/all.RVG1_CLARIN.3.cmdi.12981.1606394789/RVG1_CLARIN/"})
ds = readers.rvgj.Reader().load_dataset({"path": "/media/isse/DATA/data_original/all.RVG-J.2.cmdi.13144.1606394796/RVG-J/"})
ds = readers.sc10.Reader().load_dataset({"path": "../data_original/de/SC10/"})
ds = readers.shc.Reader().load_dataset({"path": "../data_original/de/SHC_emuDB/"})
ds = readers.si100.Reader().load_dataset({"path": "../data_original/de/SI100/"})
ds = readers.smc.Reader().load_dataset({"path": "../data_original/de/SMC/"})
ds = readers.swc.Reader().load_dataset({"path": "../data_original/de/SWC/german/"})
ds = readers.tatoeba.Reader().load_dataset({"path": "../data_original/de/tatoeba/"})
ds = readers.thorsten.Reader().load_dataset({"path": "../data_original/de/thorsten/thorsten-de/"})
ds = readers.train_speech_fr.Reader().load_dataset({"path_csv": "../data_original/fr/train_speech_fr/data.csv"})
ds = readers.tuda.Reader().load_dataset({"path": "../data_original/de/tuda/"})
ds = readers.verbmobil1.Reader().load_dataset({"path": "/media/isse/DATA/data_original/all.VM1.3.cmdi.34423.1606311255/VM1/"})
ds = readers.verbmobil2.Reader().load_dataset({"path": "../data_original/de/all.VM2.3.cmdi.37022.1606311292/VM2/"})
ds = readers.voxforge.Reader().load_dataset({"path": "../data_original/de/voxforge/"})
ds = readers.voxpopuli.Reader().load_dataset({"path": "../data_original/de/voxpopuli/"})
ds = readers.wasep.Reader().load_dataset({"path": "../data_original/de/WaSeP/"})
ds = readers.witcher3.Reader().load_dataset({"path_wavs": "C:\\Users\\Daniel\\Downloads\\w3utils - proper\\wav", "path_dialogs_txt": "C:\\Users\\Daniel\\Downloads\\w3utils - proper\\w3_scenes_de.txt"})
ds = readers.youtube.Reader().load_dataset({"path": "../DeepSpeech-German/data_original/de/musstewissen-physik/"})
ds = readers.zamia_speech.Reader().load_dataset({"path": "../data_original/de/zamia_speech/"})
ds = readers.ziptel.Reader().load_dataset({"path": "../data_original/de/ZIPTEL/"})
ds = readers.ztc_bas.Reader().load_dataset({"path": "../data_original/de/ZTC_BAS/"})
print(ds[0])

# Save dataset as wav and csv files
from corcua import writers
ds = writers.base_writer.Writer().save_dataset(ds, path="../data_prepared/de/guild2r/", sample_rate=16000, overwrite=True)

# Print some stats
from corcua import readers
ds = readers.base_reader.Reader().load_dataset({"path": "F:\\Jaco-Assistant\\DeepSpeech-German/data_original/de/guild2r/"})
from corcua import stats
ds = stats.get_duration(ds)
stats.top_key_durations(ds, key="speaker", topk=5)

# Split dataset into partitions, optionally grouped by an additional key
from corcua import utils
ds = utils.add_partitions(ds, [("train", 0.8), ("val", 0.1), ("test", 0.1)], group_key="speaker")

# Change dataset format for other speech frameworks
from corcua import readers
ds = readers.base_reader.Reader().load_dataset({"path": "F:\\Jaco-Assistant\\DeepSpeech-German/data_prepared/de/voxforge/"})
from corcua import writers
_ = writers.deepspeech.Writer().save_dataset(ds, "F:\\Jaco-Assistant\\DeepSpeech-German/data_prepared\\de\\voxforge/", sample_rate=0, overwrite=False)
_ = writers.nemo.Writer().save_dataset(ds, "F:\\Jaco-Assistant\\DeepSpeech-German/data_prepared\\de\\voxforge/", sample_rate=0, overwrite=False)
_ = writers.speechbrain.Writer().save_dataset(ds, "F:\\Jaco-Assistant\\DeepSpeech-German/data_prepared\\de\\voxforge/", sample_rate=0, overwrite=False)
```

<br>

## Citation

This library was presented in the paper about Scribosermo.
Please cite it if you found it helpful for your research or business.

```bibtex
@article{
  scribosermo,
  title={Scribosermo: Fast Speech-to-Text models for German and other Languages},
  author={Bermuth, Daniel and Poeppel, Alexander and Reif, Wolfgang},
  journal={arXiv preprint arXiv:2110.07982},
  year={2021}
}
```

<br/>

## Extending Library

**Downloaders**: Should download and extract the datsets.

**Readers**: Have to return a list of dictionaries for the files. Try to extract as much useful information as you can.
Add some progress updates if tasks are running longer than a few seconds. Required keys are: _filepath_ and _text_ OR _filepath_ and _segments_.
Samples with segments will be automatically split into single samples upon saving the dataset with a _Writer_.

```python
[
  {
    "filepath": "/path/to/audio.xyz",
    "text": "the transcription",
    "duration": 1.23,
    "arbitrary key": "some data",
    ...
  },
  {
    "filepath": "/path/to/audio.xyz",
    "segments": [
      {
        "text": "part one",
        "time_begin": 0.0,
        "time_end": 1.2,
        "arbitrary key": "some data",
      },
      {
        "text": "part two",
        "time_begin": 1.4,
        "time_end": 2.7,
      }
    ],
    "arbitrary key": "some data",
    ...
  },
  ...
]
```

**Writers**: Make sure you generate extra datafiles for the different _partitions_ in a dataset. \
The audiofiles are named automatically to remove all spaces and special characters,
if you need to get the real name and path of the audio, you can decode the names like this:
`python3 -c 'from corcua import utils; print(utils.PathSaveString().decode("KrO0XEBmFxXoGUuXmbhvg6Qk5nRhKMqts4dZ1uyxcs6y".split("_")[0]));'`

**Tests**: To make adding new datasets as easy as possible, only the base modules are tested.
Creating proper tests for each dataset or training framework would be overly complicated.
If a reader or writer really is broken due to dataset or framework updates,
I'm assuming that the users have enough programming skills to fix it and submit the patch afterwards:)

<br>

## Testing

Build container with testing tools:

```bash
docker build --progress=plain -f tests/Containerfile -t testing_corcua .

docker run --network host --rm \
  --volume `pwd`/:/corcua/ \
  -it testing_corcua
```

Run unit tests:

```bash
pip3 install --user -e /corcua/
pytest /corcua/ --cov=corcua -v -s
```

For syntax tests, check out the steps in the [gitlab-ci](.gitlab-ci.yml#L53) file.

<br>

## Games

Dialogs from computer games can be a high quality source for audio datasets.
They mostly have subtitles for the spoken texts and the dialogs normally have a more vivid pronunciation than read-only datasets.
The downside is that it is sometimes quite complicated to extract the files.

### Working

- **Elex**: Tested with GOG version on Windows 10.
  Unpack `ELEX/data/packed/c_1_de.pak` with [Elex-Resource-Manager](https://forum.worldofplayers.de/forum/threads/1512504-tool-Elex-Resource-Manager) and then convert the `.elexdlg` files.
  Extract the localization file from `c_1_na.pak`.
  Get [lianzifu](<https://forum.worldofplayers.de/forum/threads/1511649-release-Risen-3-string-table-un-packer-(lianzifu)>) and move all file into the game's root directory.
  Run `./lianzifu.exe --read-ini "data/ini/loc.ini" --read-bin "data/packed/c_1_na/localization/w_strings.bin" --save-csv` to extract the strings.

- **Gothic 1**: Tested with GOG version on Windows 10.
  Use [GothicVDFS](https://wiki.worldofgothic.de/doku.php?id=gothicvdfs) to extract the `Gothic\Data\speech*.VDF` files.
  Get the dialog script files from the [mod-developmentkit](https://www.worldofgothic.de/dl/download_28.htm).

- **Guild 2**: Tested with GOG version.
  Extract the speech files in `sfx/speech.zip` to `sfx/xtracted_speech`.

- **Kingdom Come Deliverance**: Tested with GOG version.
  Extract the speech files `German*.pak` and `IPL_German*.pak` (or a different language) in `Localization/` directory with _7zip_.

- **Skyrim**: Tested on Windows 10 with Steam version.
  Use [LazyVoiceFinder](https://www.nexusmods.com/skyrim/mods/82482?tab=description), unhide `category` and `emotion` columns and change the language of the `dialog1` column.
  Export as `.csv` and export all audio files (export one `.esp` after another, main file first, takes about 6h).
  Use [YakitoriAudioConverter] to convert `.fuz` to `.wav` files, keep the subfolders.

- **Witcher 3**: Tested on Windows with GOG version.
  Use [w3utils](https://github.com/Gizm000/Extracting-Voice-Over-Audio-from-Witcher-3) to extract the audiofiles.
  Run `.\w3unpack.exe -c -l de "E:\The Witcher 3 Wild Hunt GOTY" w3_scenes_de.txt` to extract the texts (run without arguments to see the help texts).

### Not working

- **Assassin's Creed Odyssey**: Couldn't match audio with text. \
  Use [Wwise-Unpacker](https://allsoundsasscreed.tumblr.com/post/44227512337/how-to-extract-audio-from-assassins-creed-iii) to extract audio files.
  Then use [Ubisoft FORGE and DATA Tools by Delutto](https://zenhax.com/viewtopic.php?f=9&t=9138) to extract the `Localization_Package` files
  which can be found in the `.forge` files in the `dlc` directories.
  Then use [aclocalizationpackagetool](https://forum.xentax.com/viewtopic.php?f=35&t=23769) to extract the localization file to a text file.
  Estimated duration: ~30h.

- **The Stanley Parable**: No matching transcripts found. \
  The narrators voice is freely accessible, but I couldn't open the `subtitles_english.dat` file (you can read some parts in `uft16le` encoding).

- **WatchDogs 1**: Couldn't match audio with text. \
  Steps done: Use [Disrupt](http://svn.gib.me/builds/disrupt/) and extract `common.fat` and `sound_german.fat` archives like this:
  `.\Gibbed.Disrupt.Unpack.exe "E:\Watch_Dogs\data_win64\common.fat" "C:\Users\Daniel\Downloads\wd1_unpack\common\"`.
  Use [watch-dogs-loc-tool](https://www.zenhax.com/viewtopic.php?f=12&t=14349#p60512) to extract the `.loc` files found in unpacked `common/languages/` and `patch1/unknown/languages` directories.
  Use [WDextractor](https://www.moddb.com/games/watch-dogs/downloads/wd-files-extractor) to convert the `.sbao` audio files in the `unknown/sfx/` folder.
  Estimated duration: ~30h.

- **Witcher 1**: Couldn't extract the texts. \
  Steps tested: Download [tools](https://www.moddb.com/games/the-witcher/downloads/extra-tools), execute `unbif` in Windows compatibility mode for XP-SP3 and extract all files.
  The dialogs can be viewed with `DLGView` tool, but there was no match to the audiofiles. I also couldn't decode the `.say` files which are saved next to the audiofiles.
  Estimated duration: ~20h.

- **Witcher 2**: Couldn't extract audio files. \
  Steps tested: Use tool from Witcher3 and run `.\w3unpack.exe -c -l de "E:\The Witcher 2" w2_scenes_de.txt` to extract the texts.
  Used [RedKit](https://redkitwiki.cdprojektred.com/Welcome+to+the+REDkit+Wiki) to extract the gamefiles.
  There were some english audio files but no german and the ids did not match the ones from the extracted scenes file.
