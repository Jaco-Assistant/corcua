import argparse
import math
import os
import shutil
from typing import List

import tqdm
from pydub import AudioSegment

from corcua.readers.base_reader import Reader as BaseReader
from corcua.writers.base_writer import Writer as BaseWriter

# ==================================================================================================

# Durations and paddings for train-short and sound-scape partitions
# Add a padding to make ss-files longer, but increase ts-duration instead, to reduce space usage
max_duration_ts = 9.0
split_padding_ts = 0.0
split_padding_ss = 2.0


# ==================================================================================================


def split_files(dataset: List[dict], path_target: str) -> List[dict]:
    # Add offsets to short-ds files, that the same splitting approach as for the others can be used
    tmp_ds = []
    for entry in dataset:
        if entry["offset_ending"] == 0 and entry["duration"] > max_duration_ts:
            # Split long files into multiple shorter files
            splits = math.ceil(entry["duration"] / max_duration_ts)
            split_dur = entry["duration"] / float(splits)

            for i in range(splits):
                new_entry = dict(entry)
                new_entry["offset_starts"] = i * split_dur
                new_entry["offset_ending"] = min(entry["duration"], (i + 1) * split_dur)
                tmp_ds.append(new_entry)
        else:
            tmp_ds.append(entry)

    # Now split the files if required
    splitted_ds = []
    for entry in tqdm.tqdm(tmp_ds):
        if entry["offset_ending"] == 0:
            # No split needed, just copy the audio to the target directory
            wav_name = os.path.basename(entry["filepath"])
            wav_target = os.path.join(path_target, wav_name)

            spentr = {
                "filepath": wav_target,
                "text": entry["text"],
                "birds": entry["birds"],
                "site": entry["site"],
                "partition": entry["partition"],
            }

            shutil.copyfile(entry["filepath"], wav_target)
            splitted_ds.append(spentr)

        else:
            wav_name = os.path.basename(entry["filepath"])
            wav_target = os.path.join(path_target, wav_name)

            wav_target = wav_target.replace(".wav", "_{}.wav")
            wav_target = wav_target.format(int(entry["offset_ending"]))

            # Add some extra padding data to the new audio file
            if entry["partition"] == "soundscapes":
                split_padding = split_padding_ss
            else:
                split_padding = split_padding_ts
            offset_start = max(0, entry["offset_starts"] - split_padding)
            offset_stop = min(entry["offset_ending"] + split_padding, entry["duration"])

            cut_wav_part(
                entry["filepath"],
                wav_target,
                offset_start,
                offset_stop,
            )

            spentr = {
                "filepath": wav_target,
                "text": entry["text"],
                "birds": entry["birds"],
                "site": entry["site"],
                "partition": entry["partition"],
            }
            splitted_ds.append(spentr)

    return splitted_ds


# ==================================================================================================


def cut_wav_part(
    path_source: str, path_target: str, offset_start: float, offset_stop: float
) -> None:
    start = int(offset_start * 1000)
    stop = int(offset_stop * 1000)
    newAudio = AudioSegment.from_file(path_source)
    newAudio = newAudio[start:stop]

    if not os.path.isdir(os.path.dirname(path_target)):
        os.makedirs(os.path.dirname(path_target))

    newAudio.export(path_target, format="wav")


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="Split BirdCLEF audio files")
    parser.add_argument(
        "--path_source",
        required=True,
        help="Path to data directory of dataset",
    )
    parser.add_argument(
        "--path_target",
        required=True,
        help="Path to directory to save splitted files",
    )
    args = parser.parse_args()

    shutil.rmtree(args.path_target, ignore_errors=True)
    dataset = BaseReader().load_dataset({"path": args.path_source})

    print("\nSplitting files ...")
    tpath = os.path.join(args.path_target, "audios/")
    new_ds = split_files(dataset, tpath)

    BaseWriter().save_dataset(
        new_ds, path=args.path_target, sample_rate=0, overwrite=False
    )


# ==================================================================================================

if __name__ == "__main__":
    main()
