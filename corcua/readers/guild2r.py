import os
import re
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.speech_dir = "sfx/xtracted_speech/"
        self.texts_path = "DB/Text.dbt"
        self.emotion_pattern = r"(.*E\[([A-Z_]+)\])"

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        texts_path = os.path.join(args["path"], self.texts_path)
        with open(texts_path, "r", encoding="utf-16") as file:
            lines = file.readlines()

        # Drop first 8 header lines
        lines = lines[7:]

        labels = {}
        for line in lines:
            cols = line.split("   ")
            label = cols[1].replace('"', "")
            value = cols[2].replace('"', "")
            labels[label] = value

        audiofiles = []
        speech_dir = os.path.join(args["path"], self.speech_dir)
        for dirpath, _, files in os.walk(speech_dir):
            for name in files:
                audiofiles.append(os.path.join(dirpath, name))

        dataset = []
        for fpath in audiofiles:
            fname = os.path.basename(fpath)
            fname, _ = os.path.splitext(fname)

            if fname in labels:
                text = labels[fname]
                if "%" in text:
                    continue

                speaker = fpath.replace(speech_dir, "")
                speaker = os.path.split(speaker)[0]

                m = re.search(self.emotion_pattern, text)
                if m is not None:
                    text = text.replace(m.group(1), "")
                    emotion = m.group(2)
                else:
                    emotion = ""

                entry = {
                    "emotion": emotion,
                    "filepath": fpath,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
