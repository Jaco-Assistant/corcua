import os
from typing import List

import pandas as pd

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading transcripts ...")
        ds_train = self.load_csv(os.path.join(args["path"], "metadata_train.csv"))
        ds_val = self.load_csv(os.path.join(args["path"], "metadata_val.csv"))

        # Add partitions
        for entry in ds_train:
            entry["partition"] = "train"
        for entry in ds_val:
            entry["partition"] = "val"

        dataset = ds_train
        dataset.extend(ds_val)

        for entry in dataset:
            ap = os.path.join(args["path"], "wavs/", entry["filepath"] + ".wav")
            entry["filepath"] = ap

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def load_csv(path: str) -> List[dict]:
        # The 'keep_default_na' flag is required to keep the German 0 as "null" string
        ds_pd = pd.read_csv(
            path,
            encoding="utf-8",
            sep="|",
            keep_default_na=False,
            names=["filepath", "text"],
        )
        dataset: List[dict] = ds_pd.to_dict(orient="records")
        return dataset
