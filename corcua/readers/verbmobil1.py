import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.recoding_sites = {
            "A": "Kiel",
            "C": "CMU",
            "D": "Muenchen",
            "N": "Bonn",
            "K": "Karlsruhe",
        }
        self.scenarios = {
            "K": "German, same room, no push button",
            "L": "German, separated room, no push button",
            "M": "German, separated room, push button",
            "N": "German, same room, push button",
            "G": "German, separated room, push button",
            "Q": "same as M but 'Denglisch' (Germans speaking English)",
            "R": "same as N but American English (recording site 'C') or 'Denglish' (recording site 'K')",
            "Z": "German, test recording in the scenario 'travel planning' by TP 13 Hamburg",
            "J": "same as G but extended scenario of 1995, 1996",
            "S": "same as M but mixed German-English",
            "W": "same as M but with a Wizard",
            "Y": "Japanese, same room, push button",
        }

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Reading annotations ...")
        annots = self.collect_annotations(args["path"])

        # Drop non german files
        tmplen = len(annots)
        for f in list(annots):
            key = os.path.basename(os.path.dirname(f))[0].lower()
            if key in ["q", "r", "s", "y"]:
                # Non german recording modes, see readme included in CLARINDocu
                annots.remove(f)
        print("Dropped {} non german files".format(tmplen - len(annots)))

        dataset = []
        for annot in tqdm.tqdm(annots):
            with open(annot, "r", encoding="utf-8") as file:
                annotation = json.load(file)

            text = ""
            for item in annotation["levels"][1]["items"]:
                for tag in item["labels"]:
                    if tag["name"] == "ORT":
                        t = tag["value"]

                        if "<" not in t:
                            # Don't add sounds like <ähm>
                            text += t + " "

            text = self.clean_text(text)
            if text == "":
                continue

            speaker = annot.replace("_annot.json", "")[-3:]
            dialog = os.path.basename(os.path.dirname(annot))
            turn = annotation["name"].split("_")[1]
            recording_site = self.recoding_sites[dialog[4].upper()]
            scenario = self.scenarios[dialog[0].upper()]

            entry = {
                "dialog": dialog,
                "filepath": annot.replace("_annot.json", ".wav"),
                "recording_site": recording_site,
                "scenario": scenario,
                "speaker": speaker,
                "text": text,
                "turn": turn,
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def collect_annotations(path: str) -> List[str]:
        """Collects all annotation files"""

        wavs = []
        annots = []
        for dirpath, _, files in os.walk(path):
            for name in files:
                if name.endswith(".wav"):
                    wavs.append(os.path.join(dirpath, name))
                elif name.endswith("_annot.json"):
                    annots.append(os.path.join(dirpath, name))

        # Keep only those annotations where a corresponding wav file exists
        annot_wavs = [a.replace("_annot.json", ".wav") for a in annots]
        annots_with_wavs = set(annot_wavs).intersection(set(wavs))
        annots = [aw.replace(".wav", "_annot.json") for aw in annots_with_wavs]

        return annots

    # ==============================================================================================

    @staticmethod
    def clean_text(text: str) -> str:
        """Handle special chars like umlauts"""

        text = text.strip()
        text = text.replace('"a', "ä")
        text = text.replace('"o', "ö")
        text = text.replace('"u', "ü")
        text = text.replace('"s', "ß")
        text = text.replace('"A', "Ä")
        text = text.replace('"O', "Ö")
        text = text.replace('"U', "Ü")
        text = text.replace('"S', "ẞ")
        text = text.replace("$", "")  # single letter sign
        return text
