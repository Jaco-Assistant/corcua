import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Reading annotations ...")
        annots = self.collect_annotations(args["path"])
        dataset = []
        for annot in tqdm.tqdm(annots):
            with open(annot, "r", encoding="utf-8") as file:
                annotation = json.load(file)

            text = ""
            for item in annotation["levels"][1]["items"]:
                for tag in item["labels"]:
                    if tag["name"] == "ORT":
                        t = tag["value"]

                        if "<" not in t:
                            # Don't add sounds like <ähm>
                            text += t + " "

            text = self.clean_text(text)
            if text == "":
                continue

            speaker = os.path.basename(os.path.dirname(annot))

            entry = {
                "filepath": annot.replace("_annot.json", ".wav"),
                "speaker": speaker,
                "text": text,
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def collect_annotations(path: str) -> List[str]:
        """Collects all annotation files"""

        wavs = []
        annots = []
        for dirpath, _, files in os.walk(path):
            for name in files:
                if name.endswith(".wav"):
                    wavs.append(os.path.join(dirpath, name))
                elif name.endswith("_annot.json"):
                    annots.append(os.path.join(dirpath, name))

        # Keep only those annotations where a corresponding wav file exists
        annot_wavs = [a.replace("_annot.json", ".wav") for a in annots]
        annots_with_wavs = set(annot_wavs).intersection(set(wavs))
        annots = [aw.replace(".wav", "_annot.json") for aw in annots_with_wavs]

        return annots

    # ==============================================================================================

    @staticmethod
    def clean_text(text: str) -> str:
        """Handle special chars like umlauts"""

        text = text.strip()
        text = text.replace('"a', "ä")
        text = text.replace('"o', "ö")
        text = text.replace('"u', "ü")
        text = text.replace('"s', "ß")
        text = text.replace('"A', "Ä")
        text = text.replace('"O', "Ö")
        text = text.replace('"U', "Ü")
        text = text.replace('"S', "ẞ")
        text = text.replace("$", "")  # single letter sign
        return text
