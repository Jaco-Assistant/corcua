import os
from typing import List

import pandas as pd

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        fp = os.path.dirname(os.path.realpath(__file__)) + "/"
        invp = fp + "../extras/invalid-files/common_voice.txt"
        with open(invp, "r", encoding="utf-8") as file:
            content = file.readlines()
        self.invalid_files = [c.strip() + ".mp3" for c in content]

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading transcripts ...")
        pd_test = self.read_tsv(os.path.join(args["path"], "test.tsv"))
        pd_dev = self.read_tsv(os.path.join(args["path"], "dev.tsv"))
        pd_valid = self.read_tsv(os.path.join(args["path"], "validated.tsv"))

        # Remove all dev and test files from the validated dataset and use the rest as trainset
        pd_dev_test = pd.concat([pd_dev, pd_test])["path"]
        pd_train = pd_valid[~pd_valid["path"].isin(pd_dev_test)]

        # Add partitions
        pd_test = pd_test.assign(partition="test")
        pd_dev = pd_dev.assign(partition="dev")
        pd_train = pd_train.assign(partition="train")

        pd_ds = pd.concat([pd_train, pd_dev, pd_test])
        pd_ds.rename(columns={"path": "filepath", "sentence": "text"}, inplace=True)

        # Filter invalid files
        pd_ds = pd_ds[~pd_ds.filepath.isin(self.invalid_files)]

        clip_path = os.path.join(args["path"], "clips/")
        pd_ds["filepath"] = pd_ds["filepath"].apply(lambda x: clip_path + x)

        dataset: List[dict] = pd_ds.to_dict(orient="records")
        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def read_tsv(path: str) -> pd.DataFrame:
        # The 'keep_default_na' flag is required to keep the German 0 as "null" string
        ds = pd.read_csv(path, encoding="utf-8", sep="\t", keep_default_na=False)
        return ds
