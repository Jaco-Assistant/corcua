import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        transfiles = []
        speaker_info_path = ""
        for dirpath, _, files in os.walk(args["path"]):
            for name in files:
                if name.endswith("trans.txt"):
                    transfiles.append(os.path.join(dirpath, name))
                elif name == "SPEAKERS.TXT":
                    speaker_info_path = os.path.join(dirpath, name)

        dataset = []
        for fpath in tqdm.tqdm(transfiles):
            with open(fpath, "r", encoding="utf-8") as file:
                lines = file.readlines()

            dpath = os.path.dirname(fpath)
            chapter = os.path.basename(dpath)
            sp = os.path.dirname(dpath)
            speaker = os.path.basename(sp)
            pp = os.path.dirname(sp)
            partition = os.path.basename(pp)

            for line in lines:
                name, text = line.split(" ", maxsplit=1)
                apath = os.path.join(dpath, name + ".flac")
                apath = os.path.abspath(apath)

                entry = {
                    "chapter": chapter,
                    "filepath": apath,
                    "partition": partition,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        # Extract speaker gender
        with open(speaker_info_path, "r", encoding="utf-8") as file:
            speakers = file.readlines()
        speakers = [s for s in speakers if not s.startswith(";")]
        speaker_genders = {}
        for s in speakers:
            spl = s.split("|")
            speaker_genders[spl[0].strip()] = spl[1].strip()

        # Add speaker gender
        for entry in dataset:
            entry["gender"] = speaker_genders[entry["speaker"]]

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
