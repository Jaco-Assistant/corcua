import os
import re
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path_wavs", "path_dialogs_txt"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        # Some characters in the file can't be decoded in the normally correct 'utf-8' encoding,
        # use another encoding and fix the umlaut errors later
        with open(args["path_dialogs_txt"], "r", encoding="latin-1") as file:
            lines = file.readlines()

        print("Loading texts ...")
        chunks = []
        at_chunk = False
        current_chunk: List[str] = []
        for line in tqdm.tqdm(lines):
            if re.match(r"chunk\[[0-9]+\] = {", line):
                at_chunk = True
                continue

            if line.startswith("}"):
                at_chunk = False
                chunks.append(current_chunk)
                current_chunk = []

            if at_chunk:
                current_chunk.append(line)

        print("Collecting dialogs ...")
        dialogs = []
        for chunk in tqdm.tqdm(chunks):
            voicetag = ""
            speakingTo = ""
            dialogText = ""
            dialogId = ""
            for line in chunk:
                if 'voicetag = "' in line:
                    voicetag = line.replace('voicetag = "', "").replace('",', "")
                    voicetag = voicetag.strip()
                elif 'speakingTo = "' in line:
                    speakingTo = line.replace('speakingTo = "', "").replace('",', "")
                    speakingTo = speakingTo.strip()
                elif "dialogLine = " in line:
                    dls = line.split("--")
                    dialogId = dls[0].replace("dialogLine = ", "").replace(",", "")
                    dialogId = dialogId.strip()
                    dialogText = dls[1].replace('"', "").strip()

            if dialogId != "" and dialogText != "":
                # Fix umlaut errors from our wrong encoding
                dialogText = dialogText.encode("latin-1").decode("utf-8")

                dialog = {
                    "dialogId": dialogId,
                    "listener": speakingTo,
                    "speaker": voicetag,
                    "text": dialogText,
                }
                dialogs.append(dialog)

        print("Match corresponding audio files ...")
        dataset = []
        audio_map = {}
        for f in os.listdir(args["path_wavs"]):
            audio_map[f.replace(".wav.ogg.wav", "")] = f
        for dialog in dialogs:
            if dialog["dialogId"] in audio_map:
                entry = dict(dialog)
                entry.pop("dialogId")
                entry["filepath"] = os.path.join(
                    args["path_wavs"], audio_map[dialog["dialogId"]]
                )
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
