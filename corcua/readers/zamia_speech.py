import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        for sdir in tqdm.tqdm(sorted(os.listdir(args["path"]))):
            speaker = sdir.split("-")[0]
            wavs_texts = []

            ppath = os.path.join(args["path"], sdir, "etc/PROMPTS")
            if os.path.exists(ppath):
                with open(ppath, "r", encoding="utf-8") as file:
                    promts = file.readlines()

                for promt in promts:
                    wav, text = promt.split(" ", maxsplit=1)
                    if "/mfc/" not in wav:
                        continue
                    wav = wav.split("/mfc/")[1]
                    wavs_texts.append((wav, text))

            else:
                ppath = os.path.join(args["path"], sdir, "etc/prompts-original")
                with open(ppath, "r", encoding="utf-8") as file:
                    promts = file.readlines()
                for promt in promts:
                    wav, text = promt.split(" ", maxsplit=1)
                    wavs_texts.append((wav, text))

            for wav, text in wavs_texts:
                fpath = os.path.join(args["path"], sdir, "wav/{}.wav".format(wav))

                if not os.path.exists(fpath):
                    continue

                entry = {
                    "filepath": fpath,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
