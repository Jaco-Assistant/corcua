import json
import os
import re
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading transcripts ...")
        dir_names = os.listdir(args["path"])
        dir_names = [os.path.join(args["path"], d) for d in dir_names]
        dir_names = [d for d in dir_names if os.path.isdir(d)]

        tags_pattern = re.compile("<.*?>")

        dataset = []
        dropped = 0
        for d in tqdm.tqdm(dir_names):
            files = os.listdir(d)
            files = [os.path.join(d, f) for f in files]

            annots = [f for f in files if f.endswith("_annot.json")]
            for annot in annots:
                wav = str(os.path.basename(annot).split("_")[0]) + ".wav"
                wav = os.path.join(d, wav)

                with open(annot, "r", encoding="utf-8") as file:
                    annot = json.load(file)

                trans = annot["levels"][0]["items"][0]["labels"][1]["value"]
                trans = re.sub(tags_pattern, "", trans).strip()

                # Drop files including signs for incomplete words or repetitions
                drop = False
                for s in "*~":
                    if s in trans:
                        drop = True
                        break
                if drop:
                    dropped += 1
                    continue

                entry = {
                    "filepath": wav,
                    "text": trans,
                    "speaker": os.path.basename(d),
                }
                dataset.append(entry)

        msg = "Dropped {}/{} files with speech errors"
        print(msg.format(dropped, len(dataset) + dropped))
        return dataset
