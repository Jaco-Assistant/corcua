import json
import os
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        fpath = os.path.join(args["path"], "metadata.json")
        with open(fpath, "r", encoding="utf-8") as file:
            metadata: List[dict] = json.load(file)

        dataset = []
        for item in metadata:
            apath = os.path.join(args["path"], "audio/{}.mp3".format(item["id"]))

            entry = {
                "filepath": apath,
                "license": item["license"],
                "speaker": item["speaker"],
                "text": item["text"],
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
