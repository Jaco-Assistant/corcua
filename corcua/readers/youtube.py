import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("\nLoading transcripts ...")
        align_path = os.path.join(args["path"], "alignment.json")
        with open(align_path, "r", encoding="utf-8") as file:
            aligns = json.load(file)

        dataset = []
        dropped = 0
        for a in tqdm.tqdm(aligns):
            # Drop files including signs for notes
            drop = False
            for s in "*([":
                if aligns[a].startswith(s):
                    drop = True
                    break
            if drop:
                dropped += 1
                continue

            entry = {
                "filepath": a,
                "text": aligns[a].strip(),
            }
            dataset.append(entry)

        msg = "Dropped {}/{} files with notes"
        print(msg.format(dropped, len(aligns)))

        return dataset
