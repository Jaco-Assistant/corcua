import os
from typing import List

import pandas as pd
import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path_wavs", "path_csv", "mode"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading audio files ...")
        ds_a = self.load_audiofiles(args)
        df_a = pd.DataFrame(ds_a)

        print("Loading transcripts ...")
        df_c = self.load_csv(args)
        df_c = df_c.sort_values("text")

        # Join tables
        df = pd.merge(df_a, df_c, on=["filename", "speaker"])
        df = df.drop(columns=["filename"])

        dataset: List[dict] = df.to_dict(orient="records")
        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def load_csv(args: dict) -> pd.DataFrame:
        df = pd.read_csv(args["path_csv"], encoding="utf-8", sep=",")

        # Some renaming
        df.columns = df.columns.str.strip()
        cols = {
            "^Dialogue 1.*": "text",
            "File Name": "filename",
            "Voice Type": "speaker",
            "Emotion": "emotion",
            "Category/Subtype": "gamescene",
        }
        df.columns = df.columns.to_series().replace(cols, regex=True)

        # pylint: disable=unsupported-assignment-operation disable=unsubscriptable-object
        df["filename"] = df["filename"].str.replace(".fuz", "")
        df["text"] = df["text"].str.replace('"', "")
        df["text"] = df["text"].str.strip()
        # pylint: enable=unsupported-assignment-operation

        # Drop unused columns
        df = df[df["emotion", "filename", "gamescene", "speaker", "text"]]
        # pylint: enable=unsubscriptable-object

        # Drop empty rows
        df = df[df["filename"].notna()]
        df = df[df["speaker"].notna()]
        df = df[df["text"].notna()]

        # Drop duplicates
        len_df = len(df)
        df = df.drop_duplicates(subset=["filename", "speaker"])
        print("Dropped {} duplicate entries".format(len_df - len(df)))

        # Drop non speech sounds
        len_df = len(df)
        df = df[~df["text"].str.startswith("(")]
        df = df[~df["text"].str.startswith("0x")]
        df = df[df["text"] != "..."]
        print("Dropped {} non speech sounds".format(len_df - len(df)))

        print("Found {} items in the table".format(len(df)))
        return df

    # ==============================================================================================

    @staticmethod
    def load_audiofiles(args: dict) -> List[dict]:
        """Load audio files from directory and handle invalid and duplicate items"""

        audiofiles = []
        for dirpath, _, files in os.walk(args["path_wavs"]):
            for name in files:
                audiofiles.append(os.path.join(dirpath, name))

        print("Dropping duplicates ...")
        tmp_audios1 = {}
        tmp_audios2 = {}
        for af in tqdm.tqdm(audiofiles):
            name = os.path.basename(af).replace(".wav", "")
            speaker = os.path.basename(os.path.dirname(af))
            size = os.path.getsize(af)
            id1 = name + speaker + str(size)
            id2 = name + speaker

            e = {
                "filepath": af,
                "filename": os.path.basename(af).replace(".wav", ""),
                "speaker": os.path.basename(os.path.dirname(af)),
                "size": os.path.getsize(af),
            }
            if id1 not in tmp_audios1:
                tmp_audios1[id1] = e
                if id2 not in tmp_audios2:
                    tmp_audios2[id2] = e
                else:
                    # If we found a duplicate with different file sizes, drop both items
                    tmp_audios2.pop(id2)
            else:
                # Duplicate item with equal size, assume they are the same
                tmp_audios1[id1] = e
                tmp_audios2[id2] = e

        msg = "Skipping {} duplicate files"
        print(msg.format(len(audiofiles) - len(tmp_audios2)))
        msg = "Note: {} of them had the same name but a different size"
        print(msg.format(len(tmp_audios1) - len(tmp_audios2)))

        # Drop now useless keys
        ds_audio = [v for k, v in tmp_audios2.items()]
        for e in ds_audio:
            e.pop("size")

        # Drop files marked as invalid
        if args["mode"] == "skyrim":
            fp = os.path.dirname(os.path.realpath(__file__)) + "/"
            invp = fp + "../extras/invalid-files/skyrim.txt"
            with open(invp, "r", encoding="utf-8") as file:
                content = file.readlines()
                content = [c.strip() for c in content]
                # Replace tabs with spaces -> same format if copied from table or inserted by hand
                content = [c.replace("\t", "    ") for c in content]

            len_da = len(ds_audio)
            for e in list(ds_audio):
                ikey = e["filename"] + ".fuz    " + e["speaker"]
                if ikey in content:
                    ds_audio.remove(e)
            print("Dropped {} files marked as invalid".format(len_da - len(ds_audio)))

        print("Found {} audio files".format(len(ds_audio)))
        return ds_audio
