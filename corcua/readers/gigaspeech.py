import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.jsonpath = "GigaSpeech.json"
        subsets = ["TEST", "DEV", "XS", "S", "M", "L", "XL"]
        self.subsets = ["{{{}}}".format(s) for s in subsets]

        # See: https://github.com/SpeechColab/GigaSpeech/blob/main/utils/gigaspeech_scoring.py
        conversational_filler = [
            "UH",
            "UHH",
            "UM",
            "EH",
            "MM",
            "HM",
            "AH",
            "HUH",
            "HA",
            "ER",
            "OOF",
            "HEE",
            "ACH",
            "EEE",
            "EW",
        ]
        unk_tags = ["<UNK>", "<unk>"]
        gigaspeech_punctuations = [
            "<COMMA>",
            "<PERIOD>",
            "<QUESTIONMARK>",
            "<EXCLAMATIONPOINT>",
        ]
        gigaspeech_garbage_utterance_tags = ["<SIL>", "<NOISE>", "<MUSIC>", "<OTHER>"]
        self.non_scoring_words = (
            conversational_filler
            + unk_tags
            + gigaspeech_punctuations
            + gigaspeech_garbage_utterance_tags
        )

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading labels ...")
        path = os.path.join(args["path"], self.jsonpath)
        with open(path, "r", encoding="utf-8") as file:
            data = json.load(file)

        dataset = []
        for audio in tqdm.tqdm(data["audios"]):
            audiopath = os.path.join(args["path"], audio["path"])
            if not os.path.exists(audiopath):
                continue

            category = ""
            sourcetype = ""
            if "category" in audio:
                category = audio["category"]
            if "source" in audio:
                sourcetype = audio["source"]

            entry = {
                "filepath": audiopath,
                "category": category,
                "sourcetype": sourcetype,
                "segments": [],
            }

            for segment in audio["segments"]:
                text = segment["text_tn"]

                # Remove non-scoring words
                remaining_words = []
                for word in text.split():
                    if word in self.non_scoring_words:
                        continue
                    remaining_words.append(word)
                text = " ".join(remaining_words)

                if text == "":
                    continue

                partition = ""
                for subset in self.subsets:
                    if subset in segment["subsets"]:
                        partition = subset
                        break
                partition = partition.replace("{", "")
                partition = partition.replace("}", "")

                seg = {
                    "time_begin": segment["begin_time"],
                    "time_end": segment["end_time"],
                    "partition": partition.lower(),
                    "text": text,
                }
                entry["segments"].append(seg)

            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
