import os
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.tpaths = {
            "train": "train/transcripts.txt",
            "dev": "dev/transcripts.txt",
            "test": "test/transcripts.txt",
        }

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        dataset.extend(self.load_transcript(args["path"], "test"))
        dataset.extend(self.load_transcript(args["path"], "dev"))
        dataset.extend(self.load_transcript(args["path"], "train"))

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    def load_transcript(self, base_path: str, part: str) -> List[dict]:
        tpath = os.path.join(base_path, self.tpaths[part])
        with open(tpath, "r", encoding="utf-8") as file:
            lines = file.readlines()

        dataset = []
        for line in lines:
            wav, text = line.split("\t")
            speaker, book, _ = wav.split("_")
            apath = os.path.join(base_path, part, "audio", speaker, book, wav + ".opus")

            entry = {
                "book": book,
                "filepath": apath,
                "partition": part,
                "speaker": speaker,
                "text": text.strip(),
            }
            dataset.append(entry)

        return dataset
