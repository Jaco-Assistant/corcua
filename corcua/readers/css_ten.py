import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        # Get transcripts from file
        transcripts = self.read_transcripts(args["path"])

        dataset = []
        for tdata in tqdm.tqdm(transcripts):
            fpath = os.path.join(args["path"], tdata[0])
            speaker = tdata[0].split("/")[0]
            text = tdata[2]
            traw = tdata[1]

            entry = {
                "filepath": fpath,
                "speaker": speaker,
                "text": text.strip(),
                "text-raw": traw.strip(),
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def read_transcripts(path: str) -> List[List[str]]:
        with open(os.path.join(path, "transcript.txt"), "r", encoding="utf-8") as file:
            content = file.readlines()

        transcripts = []
        for t in content:
            tl = t.split("|")
            transcripts.append(tl)

        return transcripts
