import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["speechpath", "storypath"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        wav_files = os.listdir(args["speechpath"])
        wav_files = [w for w in wav_files if w.endswith(".WAV")]

        # Load the content of all script files to speed up searching in it
        scriptlines = []
        filecounter = 0
        for dirpath, _, files in os.walk(args["storypath"]):
            for name in files:
                if not any((name.endswith(s) for s in [".d", ".D"])):
                    continue

                filecounter += 1
                path = os.path.join(dirpath, name)
                with open(path, "r", encoding="iso-8859-1") as file:
                    content = file.readlines()
                    content = [c.strip() for c in content]
                    scriptlines.extend(content)

        msg = "Loaded {} lines of dialog script code from {} files"
        print(msg.format(len(scriptlines), filecounter))

        # Drop lines without comments
        scriptlines = [s for s in scriptlines if "//" in s]
        print("Found {} lines with comments".format(len(scriptlines)))

        dataset = []
        for wfile in tqdm.tqdm(wav_files):
            searchtext = wfile.replace(".WAV", "").lower()
            for sline in scriptlines:
                if searchtext in sline.lower():
                    fpath = os.path.join(args["speechpath"], wfile)
                    text = sline.split("//")[1]

                    entry = {
                        "filepath": fpath,
                        "text": text.strip(),
                    }
                    dataset.append(entry)
                    break

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
