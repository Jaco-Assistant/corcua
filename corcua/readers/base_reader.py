import os
from pathlib import Path
from typing import List

import pandas as pd

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        if os.path.isdir(args["path"]):
            print("Loading transcripts from directory ...")
            csv_path = os.path.join(args["path"], "all.csv")
            dirpath = args["path"]
        else:
            print("Loading transcripts from file ...")
            csv_path = args["path"]
            dirpath = os.path.dirname(args["path"])

        # Last flag is required to keep the German 0 as "null" string
        ds_pd = pd.read_csv(csv_path, encoding="utf-8", sep="\t", keep_default_na=False)
        dataset: List[dict] = ds_pd.to_dict(orient="records")

        for entry in dataset:
            # Make paths absolute if they are relative
            if not os.path.isabs(entry["filepath"]):
                p = os.path.join(dirpath, entry["filepath"])
                entry["filepath"] = os.path.abspath(p)

            # Format nicely for the current system
            entry["filepath"] = str(Path(entry["filepath"]))

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
