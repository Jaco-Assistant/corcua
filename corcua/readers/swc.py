import copy
import json
import math
import os
import re
from typing import List
from xml.etree import ElementTree

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.rx_speaker = r"[sprecher|user_name]\s+=\s(.*)"
        self.rx_gender = r"[gender|geschlecht]\s+=\s(.*)"
        self.rx_dialect = r"[accent|dialekt]\s+=\s(.*)"

        self.rx_sentence = r"<n ([^>]+)>(?!<\/t><t>])"
        self.rx_token = r"<n ([^>]+)>(?!<\/t><t>])"
        self.rx_text = r"pronunciation=\"([^\"]+)\""
        self.rx_start = r"start=\"([^\"]+)\""
        self.rx_end = r"end=\"([^\"]+)\""
        self.rx_offset = r"key=\"DC.source.audio.offset\" value=\"([^\"]+)\""

        self.max_segment_length = 15
        self.min_duration = 3

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        articles = os.listdir(args["path"])

        for article in tqdm.tqdm(articles):
            dirpath = os.path.join(args["path"], article)

            entry: dict = {}
            self.add_speaker_info(entry, dirpath)
            segments = self.get_segments(dirpath)
            audios = self.get_audios(dirpath)

            if segments == [] or audios == []:
                continue

            # Match segments to audiofile
            for i, audio in enumerate(audios):
                entry = copy.deepcopy(entry)
                entry["filepath"] = audio["path"]

                segs = []
                for segment in segments:
                    if segment["time_begin"] >= audio["offset"] and (
                        i == len(audios) - 1
                        or segment["time_end"] <= audios[i + 1]["offset"]
                    ):
                        seg = {
                            "text": segment["text"],
                            "time_begin": segment["time_begin"] - audio["offset"],
                            "time_end": segment["time_end"] - audio["offset"],
                        }
                        segs.append(seg)

                entry["segments"] = segs
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    def add_speaker_info(self, entry: dict, dirpath: str) -> None:
        """Load some speaker info from the metadata file"""

        fp = os.path.join(dirpath, "audiometa.txt")
        if not os.path.exists(fp):
            entry["speaker"] = ""
            entry["dialect"] = ""
            entry["gender"] = ""
            return

        with open(fp, "r", encoding="utf-8") as file:
            content = file.read()

        speaker = re.findall(self.rx_speaker, content)
        speaker = speaker[0] if len(speaker) > 0 else ""
        entry["speaker"] = speaker

        dialect = re.findall(self.rx_dialect, content)
        dialect = dialect[0] if len(dialect) > 0 else ""
        entry["dialect"] = dialect

        genders = re.findall(self.rx_gender, content)
        gender: str = genders[0] if len(genders) > 0 else ""
        if gender.lower() in [
            "male",
            "männlich",
            "mänlich",
            "mann",
            "m",
            "männ",
            "maennlich",
        ]:
            gender = "male"
        elif gender.lower() in [
            "female",
            "weiblich",
            "f",
            "w",
            "weibl",
            "frau",
        ]:
            gender = "female"
        entry["gender"] = gender

    # ==============================================================================================

    def get_segments(self, dirpath: str) -> List[dict]:
        """Get all segments for a file and combine them to longer utterances"""

        fp = os.path.join(dirpath, "aligned.swc")
        if not os.path.exists(fp):
            return []

        xml = ElementTree.parse(fp)
        tree = xml.getroot()

        # Remove all <ignored> tags
        for parent in tree.findall(".//ignored/.."):
            for element in parent.findall("ignored"):
                parent.remove(element)
        for parent in tree.findall(".//extra/.."):
            for element in parent.findall("extra"):
                parent.remove(element)

        s_tags = tree.findall(".//s")
        sents = [ElementTree.tostring(s, encoding="unicode") for s in s_tags]

        segements = []
        for sent in sents:
            segements.append(self.extract_segments(sent))
        segements = [s for s in segements if s != []]

        combined_segs = []
        for segs in segements:
            comb_segs = self.combine_segments(segs)
            combined_segs.extend(comb_segs)

        return combined_segs

    # ==============================================================================================

    @staticmethod
    def get_audios(dirpath: str) -> List[dict]:
        """Get audiofiles with their offsets, sometimes the audio is split into multiple files"""

        audios: List[dict]
        fp = os.path.join(dirpath, "info.json")
        if not os.path.exists(fp):
            return []

        with open(fp, "r", encoding="utf-8") as file:
            content = json.load(file)

        if len(content["audio_files"]) == 1:
            if not os.path.exists(os.path.join(dirpath, "audio.ogg")):
                return []
            audios = [{"path": "audio.ogg", "offset": 0.0}]

        else:
            audios = []
            for i in range(len(content["audio_files"])):
                if "offset" not in content["audio_files"][i]:
                    continue

                fp = "audio{}.ogg".format(i + 1)
                if not os.path.exists(os.path.join(dirpath, fp)):
                    continue

                info = {
                    "path": fp,
                    "offset": content["audio_files"][i]["offset"],
                }
                audios.append(info)

        for audio in audios:
            audio["path"] = os.path.join(dirpath, audio["path"])

        return audios

    # ==============================================================================================

    def extract_segments(self, textline: str) -> List[dict]:
        """Extract aligned segements infos from label file textline"""

        segments = []
        tokens = re.findall(self.rx_token, textline)
        for i, token in enumerate(tokens):
            segment = {
                "text": "",
                "time_begin": -1.0,
                "time_end": -1.0,
                "idx": i,
            }

            tmatch = re.findall(self.rx_text, token)
            if len(tmatch) == 1:
                segment["text"] = tmatch[0]

            tmatch = re.findall(self.rx_start, token)
            if len(tmatch) == 1:
                segment["time_begin"] = int(tmatch[0]) / 1000.0

            tmatch = re.findall(self.rx_end, token)
            if len(tmatch) == 1:
                segment["time_end"] = int(tmatch[0]) / 1000.0

            if segment["text"] != "":
                segments.append(segment)
            else:
                break

        if segments == []:
            return segments

        # In some cases an intermediate transcription has missing timestamps, to ensure correct
        # appending of tokens, check if the following token has a timestamp again and use this one
        # to fix it, else ignore it.
        filtered_segments = []
        for i, seg in enumerate(segments):
            if seg["time_begin"] != -1 and seg["time_end"] != -1:
                filtered_segments.append(seg)
            else:
                if 0 < i < len(segments) - 2:
                    seg["time_begin"] = segments[i - 1]["time_end"]
                    seg["time_end"] = segments[i + 1]["time_begin"]

                    if seg["time_begin"] != -1 and seg["time_end"] != -1:
                        filtered_segments.append(seg)
                    else:
                        continue
                else:
                    # Just ignore missing timestamps at the first or last token
                    continue

        return filtered_segments

    # ==============================================================================================

    def combine_segments(self, segments: List[dict]) -> List[dict]:
        """Combine multiple short segments to a longer utterance"""

        # Get total segment length and calculate number of splits
        total_length = segments[-1]["time_end"] - segments[0]["time_begin"]
        num_splits = math.ceil(total_length / self.max_segment_length)
        splitsize = total_length / num_splits

        new_seg = {
            "text": segments[0]["text"],
            "time_begin": segments[0]["time_begin"],
            "time_end": segments[0]["time_end"],
        }

        new_segments = []
        start_new = False
        last_idx = 0
        i = -1
        for i, segment in enumerate(segments[1:], start=1):
            start_new = False

            # Check if segments follow one another
            if segment["idx"] == last_idx + 1:
                if (
                    segment["time_end"] - new_seg["time_begin"] < splitsize
                    or i == len(segments) - 1
                ):
                    # Add last entry even if size gets a bit larger
                    new_seg["text"] = new_seg["text"] + " " + segment["text"]
                    new_seg["time_end"] = segment["time_end"]
                    last_idx = segment["idx"]
                else:
                    start_new = True
            else:
                start_new = True

            if start_new:
                new_segments.append(dict(new_seg))
                new_seg["text"] = segment["text"]
                new_seg["time_begin"] = segment["time_begin"]
                new_seg["time_end"] = segment["time_end"]
                last_idx = segment["idx"]

        if not start_new or i == len(segments) - 1:
            # Add last segment concatenation
            new_segments.append(dict(new_seg))

        # Add a small offset before and after to improve inaccurate alignments
        extra_time = 0.05
        for seg in new_segments:
            seg["time_begin"] = max(0, seg["time_begin"] - extra_time)
            seg["time_end"] = seg["time_end"] + extra_time

        # Filter out too short files
        new_segments = [
            seg
            for seg in new_segments
            if seg["time_end"] - seg["time_begin"] > self.min_duration
        ]

        return new_segments
