import os
from typing import List, Tuple

import tqdm

from .. import utils
from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        fp = os.path.dirname(os.path.realpath(__file__)) + "/"
        invp = fp + "../extras/invalid-files/voxforge.txt"
        with open(invp, "r", encoding="utf-8") as file:
            content = file.readlines()
        self.invalid_files = [c.strip() for c in content]

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        for sdir in tqdm.tqdm(sorted(os.listdir(args["path"]))):
            speaker = sdir.split("-")[0]
            wavs_texts = []

            ppath = os.path.join(args["path"], sdir, "etc/PROMPTS")
            if os.path.exists(ppath):
                encoding = utils.get_encoding_type(ppath)
                with open(ppath, "r", encoding=encoding) as file:
                    promts = file.readlines()

                for promt in promts:
                    wav, text = promt.split(" ", maxsplit=1)
                    if "/mfc/" not in wav:
                        continue
                    wav = wav.split("/mfc/")[1]
                    wavs_texts.append((wav, text))

            else:
                ppath = os.path.join(args["path"], sdir, "etc/prompts-original")
                encoding = utils.get_encoding_type(ppath)
                with open(ppath, "r", encoding=encoding) as file:
                    promts = file.readlines()
                for promt in promts:
                    wav, text = promt.split(" ", maxsplit=1)
                    wavs_texts.append((wav, text))

            for wav, text in wavs_texts:
                fpath = os.path.join(args["path"], sdir, "wav/{}.wav".format(wav))

                if not os.path.exists(fpath):
                    continue

                if "{}-{}".format(sdir, wav) in self.invalid_files:
                    continue

                rpath = os.path.join(args["path"], sdir, "etc/README")
                gender, age, dialect, mic = self.get_metadata(rpath)

                entry = {
                    "age": age,
                    "dialect": dialect,
                    "gender": gender,
                    "filepath": fpath,
                    "microphone": mic,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def get_metadata(path: str) -> Tuple[str, str, str, str]:
        gender = ""
        age = ""
        dialect = ""
        mic = ""

        if os.path.exists(path):
            encoding = utils.get_encoding_type(path)
            with open(path, "r", encoding=encoding) as file:
                metadata = file.readlines()

            for mdata in metadata:
                if "Gender:" in mdata:
                    md = mdata.replace("Gender:", "").strip()
                    if md in ["Männlich", "MÃ¤nnlich", "male;"]:
                        gender = "male"
                    elif md in ["Weiblich", "female;"]:
                        gender = "female"

                elif "Age Range:" in mdata:
                    md = mdata.replace("Age Range:", "").strip()
                    if md not in ["unbekannt", "Bitte w채hlen"]:
                        age = md

                elif "Pronunciation dialect:" in mdata:
                    md = mdata.replace("Pronunciation dialect:", "").strip()
                    if md in ["Südwestdeutschland", "SÃ¼dwestdeutschland"]:
                        dialect = "Südwestdeutschland"
                    elif md in ["Österreich", "Ã–sterreich"]:
                        dialect = "Österreich"
                    elif md == "standard German.":
                        dialect = "Hochdeutsch"
                    elif "Hamburg" in md:
                        dialect = "Hamburg"
                    elif md == "südl. Ostdeutschland":
                        dialect = "Südostdeutschland"
                    elif md in [
                        "Westdeutschland",
                        "Norddeutschland",
                        "Schweiz",
                        "Bayern",
                        "Berlin",
                    ]:
                        dialect = md

                elif "Microphone type: " in mdata:
                    md = mdata.replace("Microphone type: ", "").strip()
                    if md == "Headset-Mikro (am KopfhÃ¶rer)":
                        mic = "Headset-Mikro (am Kopfhörer)"
                    elif md in ["Webcam-micro", "Webcam-Micro"]:
                        mic = "Webcam-Mikro"
                    elif md in ["Anderes Mikro", "unbekannt", "Bitte w채hlen"]:
                        pass
                    else:
                        mic = md

        return (gender, age, dialect, mic)
