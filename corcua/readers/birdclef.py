import os
from typing import List

import pandas as pd
import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    @staticmethod
    def load_short_dataset(path: str) -> List[dict]:
        shortdir = os.path.join(path, "train_short_audio/")

        audiofiles = []
        for dirpath, _, files in os.walk(shortdir):
            for name in files:
                audiofiles.append(os.path.join(dirpath, name))

        dataset = []
        for fpath in tqdm.tqdm(audiofiles):
            label = os.path.dirname(fpath)
            label = os.path.split(label)[1]

            entry = {
                "filepath": fpath,
                "text": "",
                "birds": label,
                "site": "",
                "offset_starts": 0,
                "offset_ending": 0,
                "partition": "trainshort",
            }
            dataset.append(entry)

        return dataset

    # ==============================================================================================

    @staticmethod
    def load_soundscape_dataset(path: str) -> List[dict]:
        audiodir = os.path.join(path, "train_soundscapes/")
        csv_path = os.path.join(path, "train_soundscape_labels.csv")

        ds_pd = pd.read_csv(csv_path, encoding="utf-8", sep=",", keep_default_na=False)
        ds_pd = ds_pd[["site", "audio_id", "seconds", "birds"]]
        dataset: List[dict] = ds_pd.to_dict(orient="records")

        audiofiles = os.listdir(audiodir)

        for entry in dataset:
            astart = "{}_".format(entry["audio_id"])
            apaths = [f for f in audiofiles if f.startswith(astart)]
            apath = os.path.join(audiodir, apaths[0])

            entry["filepath"] = apath
            entry.pop("audio_id")

            entry["offset_ending"] = entry["seconds"]
            entry["offset_starts"] = entry["seconds"] - 5
            entry.pop("seconds")

            entry["text"] = ""
            entry["partition"] = "soundscape"

        return dataset

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        short_dataset = self.load_short_dataset(args["path"])
        dataset.extend(short_dataset)

        # Every file in this dataset will be converted to wav multiple times and overwrites the old
        # conversion, which is not very efficient, but was simpler for implementation, and the time
        # overhead is only about 10 minutes
        sscapes_dataset = self.load_soundscape_dataset(args["path"])
        dataset.extend(sscapes_dataset)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
