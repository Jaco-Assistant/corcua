import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        all_files = []
        for dirpath, _, files in os.walk(args["path"]):
            for file in files:
                all_files.append([dirpath, file])

        dataset = []
        for dirpath, file in tqdm.tqdm(all_files):
            filepath = os.path.join(dirpath, file)
            speaker = os.path.basename(dirpath)
            transcription = os.path.splitext(os.path.basename(filepath))[0]

            entry = {
                "filepath": filepath,
                "speaker": speaker,
                "text": transcription.strip(),
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
