import os
import re
from typing import List

import tqdm
import yaml

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.partitions = ["test", "train", "valid"]

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path", "language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        for part in tqdm.tqdm(self.partitions):
            data = self.read_data(args["path"], part, args["language"])
            dataset.extend(data)

        # Filter out sound classifications
        for entry in dataset:
            text = entry["text"]
            text = re.sub(r"\(.+\)", " ", text)
            text = re.sub(r"\[.+\]", " ", text)
            text = re.sub(r"\s[\s]+", " ", text)
            text = text.strip()
            entry["text"] = text

        # Drop empty texts
        dataset = [e for e in dataset if e["text"] not in ["", " "]]

        # Combine entries if they use the same audio file
        dataset = self.reorder_entries(dataset)

        # Update audio path
        for entry in dataset:
            fp = entry["filepath"].replace(".wav", ".flac")
            fp = os.path.join(args["path"], fp)
            entry["filepath"] = os.path.abspath(fp)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==================================================================================================

    @staticmethod
    def read_data(path: str, part: str, lang: str) -> List[dict]:
        path_yaml = os.path.join(path, part, "txt/{}.yaml".format(part))
        with open(path_yaml, "r", encoding="utf-8") as file:
            sections: List[dict] = yaml.load(file, Loader=yaml.SafeLoader)

        path_text = os.path.join(path, part, "txt/{}.{}".format(part, lang))
        with open(path_text, "r", encoding="utf-8") as file:
            texts = file.readlines()

        # Merge sections and texts
        for i, sec in enumerate(sections):
            sec["text"] = texts[i].strip()
            sec["wav"] = "{}/wav/".format(part) + sec["wav"]

        return sections

    # ==================================================================================================

    @staticmethod
    def reorder_entries(dataset: List[dict]) -> List[dict]:
        """Combine entries using the same audiofile to a single entry with segments"""

        # Map by audiofile
        filemap: dict = {}
        for entry in dataset:
            if entry["wav"] in filemap:
                filemap[entry["wav"]].append(entry)
            else:
                filemap[entry["wav"]] = [entry]

        # Combine segments into single sample
        new_dataset = []
        for filename, entries in filemap.items():
            entry = {
                "filepath": filename,
                "segments": [],
            }

            for seg in entries:
                segment = {
                    "speaker": seg["speaker_id"],
                    "text": seg["text"],
                    "time_begin": seg["offset"],
                    "time_end": seg["offset"] + seg["duration"],
                }

                # Add some extra time, last word often was not fully complete
                segment["time_begin"] = max(0, segment["time_begin"] - 0.05)
                segment["time_end"] = segment["time_end"] + 0.1

                entry["segments"].append(segment)
            new_dataset.append(entry)

        return new_dataset
