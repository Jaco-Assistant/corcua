import os
from typing import List

import pandas as pd

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading transcripts ...")
        mpath = os.path.join(args["path"], "metadata.tsv")
        dataset = self.read_data(mpath)

        for entry in dataset:
            # Construct real filepath
            year = entry["session_id"][0:4]
            fp = "original/{}/{}_original.ogg".format(year, entry["session_id"])
            fp = os.path.join(args["path"], fp)
            entry["filepath"] = os.path.abspath(fp)
            entry.pop("session_id")

            # Fix gender
            if entry["gender"] == "na":
                entry["gender"] = ""

            # Strip spaces
            entry["text"] = entry["text"].strip()

        # Remove empty segments
        dataset = [e for e in dataset if e["text"] != ""]

        # Combine entries if they use the same audio file
        dataset = self.reorder_entries(dataset)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==================================================================================================

    @staticmethod
    def read_data(path: str) -> List[dict]:
        """Load label table and return as dictionary"""

        ds_pd = pd.read_csv(path, encoding="utf-8", sep="|", keep_default_na=False)

        # Drop some keys and samples that are marked invalid
        ds_pd = ds_pd[
            [
                "session_id",
                "speaker_id",
                "original_text",
                "start_time",
                "end_time",
                "split",
                "gender",
            ]
        ]
        ds_pd = ds_pd[ds_pd["split"] != "invalid"]

        # Rename some columns
        ds_pd = ds_pd.rename(
            {
                "speaker_id": "speaker",
                "original_text": "text",
                "start_time": "time_begin",
                "end_time": "time_end",
                "split": "partition",
            },
            axis="columns",
        )

        dataset: List[dict] = ds_pd.to_dict(orient="records")
        return dataset

    # ==================================================================================================

    @staticmethod
    def reorder_entries(dataset: List[dict]) -> List[dict]:
        """Combine entries using the same audiofile to a single entry with segments"""

        # Map by audiofile
        filemap: dict = {}
        for entry in dataset:
            if entry["filepath"] in filemap:
                filemap[entry["filepath"]].append(entry)
            else:
                filemap[entry["filepath"]] = [entry]

        # Combine segments into single sample
        new_dataset = []
        for filename, entries in filemap.items():
            entry = {
                "filepath": filename,
                "segments": [],
            }

            for seg in entries:
                segment = dict(seg)
                segment.pop("filepath")

                entry["segments"].append(segment)
            new_dataset.append(entry)

        return new_dataset
