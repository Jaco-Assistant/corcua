import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.day_emotions = {
            "f1": "happy",
            "f2": "stressed out",
            "f3": "aggressive",
            "f4": "sad",
            "f5": "relaxed",
            "f6": "tired",
            "f7": "depressive",
            "f8": "desperate",
            "f9": "rested",
            "f10": "frolicsome",
        }
        self.test_emotions = {
            "r1": "relaxed",
            "r2": "bored",
            "r3": "exited",
            "r4": "nervous",
        }

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Reading annotations ...")
        annots = self.collect_annotations(args["path"])
        dataset = []
        for annot in tqdm.tqdm(annots):
            with open(annot, "r", encoding="utf-8") as file:
                annotation = json.load(file)

            text = ""
            for item in annotation["levels"][1]["items"]:
                for tag in item["labels"]:
                    if tag["name"] == "word":
                        t = tag["value"]

                        if "<" not in t and "#" not in t:
                            # Don't add sounds like <ähm> or comments like #GARBAGE#
                            text += t + " "

            text = self.clean_text(text)
            if text == "":
                continue

            speaker = os.path.basename(os.path.dirname(annot))
            gender = ""
            age = ""
            school_region = ""
            weather = ""
            breath_alcohol = ""
            blood_alcohol = ""
            day_emotion = ""
            test_emotion = ""
            drinking_habit = ""
            for item in annotation["levels"][0]["items"]:
                for tag in item["labels"]:
                    if tag["name"] == "sex":
                        if tag["value"] == "F":
                            gender = "female"
                        elif tag["value"] == "M":
                            gender = "male"
                    if tag["name"] == "age":
                        age = tag["value"]
                    if tag["name"] == "acc":
                        school_region = tag["value"]
                    if tag["name"] == "wea":
                        weather = tag["value"]
                    if tag["name"] == "aak":
                        breath_alcohol = tag["value"]
                    if tag["name"] == "bak":
                        blood_alcohol = tag["value"]
                    if tag["name"] == "ges":
                        day_emotion = self.day_emotions[tag["value"]]
                    if tag["name"] == "ces":
                        test_emotion = self.test_emotions[tag["value"]]
                    if tag["name"] == "drh":
                        drinking_habit = tag["value"]

            entry = {
                "age": age,
                "blood_alcohol": blood_alcohol,
                "breath_alcohol": breath_alcohol,
                "day_emotion": day_emotion,
                "drinking_habit": drinking_habit,
                "filepath": annot.replace("_annot.json", ".wav"),
                "gender": gender,
                "school_region": school_region,
                "speaker": speaker,
                "test_emotion": test_emotion,
                "text": text,
                "weather": weather,
            }
            dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset

    # ==============================================================================================

    @staticmethod
    def collect_annotations(path: str) -> List[str]:
        """Collects all annotation files"""

        wavs = []
        annots = []
        for dirpath, _, files in os.walk(path):
            for name in files:
                if name.endswith(".wav"):
                    wavs.append(os.path.join(dirpath, name))
                elif name.endswith("_annot.json"):
                    annots.append(os.path.join(dirpath, name))

        # Keep only those annotations where a corresponding wav file exists
        annot_wavs = [a.replace("_annot.json", ".wav") for a in annots]
        annots_with_wavs = set(annot_wavs).intersection(set(wavs))
        annots = [aw.replace(".wav", "_annot.json") for aw in annots_with_wavs]

        return annots

    # ==============================================================================================

    @staticmethod
    def clean_text(text: str) -> str:
        """Handle special chars like umlauts"""

        text = text.strip()
        text = text.replace('"a', "ä")
        text = text.replace('"o', "ö")
        text = text.replace('"u', "ü")
        text = text.replace('"s', "ß")
        text = text.replace('"A', "Ä")
        text = text.replace('"O', "Ö")
        text = text.replace('"U', "Ü")
        text = text.replace('"S', "ẞ")
        text = text.replace("$", "")  # single letter sign
        text = text.replace("\\", "")
        return text
