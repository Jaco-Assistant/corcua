import os
from pathlib import Path
from typing import List

import pandas as pd

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path_csv"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        print("Loading transcripts ...")
        # The 'keep_default_na' flag is required to keep the German 0 as "null" string
        ds_pd = pd.read_csv(
            args["path_csv"], encoding="utf-8", sep=",", keep_default_na=False
        )
        dataset: List[dict] = ds_pd.to_dict(orient="records")

        for entry in dataset:
            # Rename keys
            entry["filepath"] = entry["wav_filename"]
            entry["text"] = str(entry["transcript"])
            entry.pop("wav_filename")
            entry.pop("transcript")

            # Make paths absolute if they are relative
            if not os.path.isabs(entry["filepath"]):
                p = os.path.join(os.path.dirname(args["path_csv"]), entry["filepath"])
                entry["filepath"] = p

            # Format nicely for the current system
            entry["filepath"] = str(Path(entry["filepath"]))

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
