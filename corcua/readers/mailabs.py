import json
import os
from typing import List

import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        fp = os.path.dirname(os.path.realpath(__file__)) + "/"
        invp = fp + "../extras/invalid-files/mailabs.txt"
        with open(invp, "r", encoding="utf-8") as file:
            content = file.readlines()
        self.invalid_files = [c.strip() for c in content]

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        json_files = []
        for dirpath, _, files in os.walk(args["path"]):
            for name in files:
                if name == "metadata_mls.json":
                    json_files.append(os.path.join(dirpath, name))

        dataset = []
        for fpath in tqdm.tqdm(json_files):
            with open(fpath, "r", encoding="utf-8") as file:
                metadata = json.load(file)

            dpath = os.path.dirname(fpath)
            book = os.path.basename(dpath)
            sp = os.path.dirname(dpath)
            speaker = os.path.basename(sp)
            pp = os.path.dirname(sp)
            gender = os.path.basename(pp)

            for item in metadata:
                text = metadata[item]["clean"]
                apath = os.path.join(dpath, "wavs/", item)

                if not os.path.exists(apath):
                    continue

                wav_name = item.replace(".wav", "")
                if (
                    wav_name in self.invalid_files
                    or "{}-{}".format(speaker, wav_name) in self.invalid_files
                ):
                    continue

                entry = {
                    "book": book,
                    "filepath": apath,
                    "gender": gender,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
