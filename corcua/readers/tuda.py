import os
import re
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.subsets = ["train", "dev", "test"]
        self.mics = [
            "Samson",
            "Realtek",
            "Kinect-RAW",
            "Kinect-Beam",
            "Yamaha",
            "Microsoft-Kinect-Raw",
        ]

        self.rx_speaker = r"<speaker_id>(.*)<\/speaker_id>"
        self.rx_gender = r"<gender>(.*)<\/gender>"
        self.rx_ageclass = r"<ageclass>(.*)<\/ageclass>"
        self.rx_region = r"<bundesland>(.*)<\/bundesland>"
        self.rx_native = r"<muttersprachler>(.*)<\/muttersprachler>"
        self.rx_text = r"<cleaned_sentence>(.*)<\/cleaned_sentence>"

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        dataset = []
        for subset in self.subsets:
            dirpath = os.path.join(args["path"], subset)
            files = os.listdir(dirpath)
            files = [f for f in files if f.endswith(".xml")]

            for fp in files:
                fp = os.path.join(dirpath, fp)
                with open(fp, "r", encoding="utf-8") as file:
                    content = file.read()

                speaker = re.findall(self.rx_speaker, content)[0]
                gender = re.findall(self.rx_gender, content)[0]
                ageclass = re.findall(self.rx_ageclass, content)[0]
                region = re.findall(self.rx_region, content)[0]
                text = re.findall(self.rx_text, content)[0]
                is_native = re.findall(self.rx_native, content)[0]
                is_native = bool(is_native in ["Ja", "ja"])

                for mic in self.mics:
                    apath = fp.replace(".xml", "_{}.wav".format(mic))

                    if not os.path.exists(apath):
                        continue

                    if mic == "Realtek":
                        # Exclude all recordings with Realtek microphone, because they're not in
                        # the official test set, see chapter 3.2 of the paper:
                        # https://edoc.sub.uni-hamburg.de/informatik/volltexte/2018/243/pdf/milde_koehn_german_asr.pdf
                        partition = "excluded-" + subset
                    else:
                        partition = subset

                    entry = {
                        "ageclass": ageclass,
                        "filepath": apath,
                        "gender": gender,
                        "is_native": is_native,
                        "microphone": mic,
                        "partition": partition,
                        "region": region,
                        "speaker": speaker,
                        "text": text,
                    }
                    dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
