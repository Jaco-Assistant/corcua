import os
from typing import List

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.base_dir = "Localization/"
        self.texts_path = "Localization/{}_xml/text_ui_dialog.xml"
        self.speech_dirs = [
            "IPL_{}",
            "IPL_{}_HD",
            "{}",
            "{}_DLC2",
            "{}_DLC3",
            "{}_DLC4",
            "{}-part0",
            "{}-part1",
            "{}_HD-part0",
            "{}_HD-part1",
        ]

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["path", "language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        texts_path = os.path.join(args["path"], self.texts_path)
        texts_path = texts_path.format(args["language"].title())
        with open(texts_path, "r", encoding="utf-8") as file:
            lines = file.readlines()

        # Drop header and footer lines
        lines = lines[1:-1]

        # Load texts
        labels = {}
        for line in lines:
            line = line.strip()
            line = line.replace("<Row>", "").replace("</Row>", "")
            line = line.replace("<Cell>", "")
            line = line.strip()
            cols = line.split("</Cell>")
            cols = [c for c in cols if c != ""]
            if len(cols) == 3:
                labels[cols[0]] = {
                    "name": cols[0],
                    "text": cols[2].strip(),
                }

        # Find audio files
        # If there are duplicates, overwrite the old path
        audiofiles = {}
        base_dir = os.path.join(args["path"], self.base_dir)
        speech_dirs = [os.path.join(base_dir, sdir) for sdir in self.speech_dirs]
        speech_dirs = [sdir.format(args["language"].title()) for sdir in speech_dirs]
        for sdir in speech_dirs:
            if not os.path.isdir(sdir):
                continue
            for dirpath, _, files in os.walk(sdir):
                for name in files:
                    fname, _ = os.path.splitext(name)
                    audiofiles[fname] = os.path.join(dirpath, name)

        # Combine audios with texts
        dataset = []
        for fname, fpath in audiofiles.items():
            fkey = "_".join(fname.split("_")[1:])

            if fkey in labels:
                item = labels[fkey]

                if len(item["text"]) == 0:
                    continue
                if item["text"] in ["&lt;...&gt;", "&lt;…&gt;"]:
                    continue

                entry = {
                    "filepath": fpath,
                    "speaker": fname.split("_")[0],
                    "text": item["text"],
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
