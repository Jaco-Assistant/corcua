import os
from typing import List

import pandas as pd
import tqdm

from .abstract_reader import AbstractReader

# ==================================================================================================


class Reader(AbstractReader):
    def __init__(self) -> None:
        self.str_langs = {
            "de": "German_Text",
            "en": "English_Text",
            "pl": "Polish_Text",
        }
        self.wav_langs = {
            "de": "_German",
            "en": "_English",
            "pl": "_Polish",
        }

    # ==============================================================================================

    @staticmethod
    def hash_name(f: str) -> str:
        """See: https://forum.worldofplayers.de/forum/threads/1513519-release-ELEX-string-table-un-packer-%28lianzifu%29?p=25857847&viewfull=1#post25857847"""

        def hash_djb2(s: str) -> int:
            """From: https://gist.github.com/mengzhuo/180cd6be8ba9e2743753"""
            hashval = 5381
            for x in s:
                hashval = ((hashval << 5) + hashval) + ord(x)
            return hashval & 0xFFFFFFFF

        h = hex(hash_djb2(f.lower()))
        h = h[2:]
        return h

    # ==============================================================================================

    def load_dataset(self, args: dict) -> List[dict]:
        req_args = ["speechpath", "stringpath", "language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        wav_files = os.listdir(args["speechpath"])
        wav_files = [w for w in wav_files if w.endswith(".wav")]

        strings = pd.read_csv(
            args["stringpath"],
            encoding="utf-8",
            sep="|",
            keep_default_na=False,
            error_bad_lines=False,
        )

        # Drop unused columns and rows
        key_lang = self.str_langs[args["language"]]
        strings = strings[["ID", key_lang]]
        strings = strings[strings[key_lang] != ""]

        dataset = []
        for wfile in tqdm.tqdm(wav_files):
            # Get the file identifier
            name = wfile.replace(".wav", "")
            name = name.replace(self.wav_langs[args["language"]], "")

            # Find the matching transcription
            hashid = self.hash_name(name)
            row = strings[strings["ID"].str.match(hashid)]

            if len(row) == 1:
                fpath = os.path.join(args["speechpath"], wfile)
                text = row[key_lang].item()
                speaker = name.split("_")[1]

                entry = {
                    "filepath": fpath,
                    "speaker": speaker,
                    "text": text.strip(),
                }
                dataset.append(entry)

        print("Did successfully read {} items".format(len(dataset)))
        return dataset
