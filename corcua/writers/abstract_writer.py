import copy
import gc
import itertools
import math
import multiprocessing as mp
import os
import shutil
import time
import uuid
from abc import ABCMeta, abstractmethod
from concurrent import futures
from functools import partial
from io import BytesIO
from typing import Any, Callable, List, Tuple

import psutil
import soundfile as sf
import tqdm
from pydub import AudioSegment, exceptions

# ==================================================================================================


class AbstractWriter:
    __metaclass__ = ABCMeta

    # ==============================================================================================

    @abstractmethod
    def save_dataset(
        self, dataset: List[dict], path: str, sample_rate: int, overwrite: bool
    ) -> List[dict]:
        """If not overwritten by the base-class, this function creates the dataset folder and
        optionally empties it. Else it also should save the annotation files and in some cases
        convert the audio files to the specific formats."""

        if len(dataset) == 0:
            raise ValueError("Dataset is empty!")

        if os.path.exists(path):
            if overwrite:
                shutil.rmtree(path, ignore_errors=True)
            else:
                msg = "WARNING: The given directory already exists: {}"
                print(msg.format(path))

        os.makedirs(path, exist_ok=True)
        return dataset

    # ==============================================================================================

    @staticmethod
    def check_file_exists(path: str) -> None:
        if not os.path.exists(path):
            msg = "This file does not exist: {}".format(path)
            raise FileNotFoundError(msg)

    # ==============================================================================================

    @staticmethod
    def get_real_cpu_count() -> int:
        """Get number of available CPUs

        In difference to "mp.cpu_count()" this also works on compute nodes where the process can
        allocate only a subset of the complete CPU amount.
        """

        cpu_count = len(os.sched_getaffinity(0))
        return cpu_count

    # ==============================================================================================

    @staticmethod
    def improved_pool_map(
        func: Callable, items: List[Any], parallel: List[bool]
    ) -> List[Any]:
        """Reduce memory requirements of large parallel processes, by executing them in streamed
        chunks. Allows a combination of alternating parallel and single threaded execution.
        """

        parallel_indices = [i for i, p in enumerate(parallel) if p]
        single_indices = [i for i, p in enumerate(parallel) if not p]

        items_left = len(items)
        items_left_parallel = len(parallel_indices)
        items_left_single = len(single_indices)

        items_parallel = iter(((i, items[i]) for i in parallel_indices))
        items_single = iter(((i, items[i]) for i in single_indices))

        process_count = AbstractWriter.get_real_cpu_count()
        parallel_queue_size = process_count * 10
        if items_left_single > 0:
            single_queue_size = math.ceil(
                items_left_single / math.ceil(items_left_parallel / parallel_queue_size)
            )
        else:
            single_queue_size = 0

        msg = (
            "Processing {} items with {} threads and {} items with 1 thread"
            + " in interleaved ({}:{}) batches ..."
        )
        print(
            msg.format(
                items_left_parallel,
                process_count,
                items_left_single,
                parallel_queue_size,
                single_queue_size,
            )
        )
        results = [None] * items_left
        pbar = tqdm.tqdm(total=items_left)

        while items_left > 0:
            # Start with a batch of parallel jobs.
            batch_left = parallel_queue_size
            batch_items = []
            while batch_left > 0:
                try:
                    item = next(items_parallel)
                except StopIteration:
                    break
                batch_items.append(item)
                batch_left -= 1

            batch_left = len(batch_items)
            jobs = {}
            with futures.ProcessPoolExecutor(process_count) as executor:
                for idx, item in batch_items:
                    job = executor.submit(func, item)
                    jobs[job] = idx

                while batch_left > 0:
                    # Get the completed jobs whenever they are done
                    for job in futures.as_completed(jobs):
                        # Current job finished
                        items_left -= 1
                        items_left_parallel -= 1
                        batch_left -= 1
                        pbar.update(1)

                        # Get the result and the idx of this job
                        result = job.result()
                        idx = jobs[job]
                        results[idx] = result

                        # Delete the job from the dict as we don't need to store it anymore
                        jobs.pop(job)
                        del job
                        gc.collect()

                    # Sleep a short time before checking if any new jobs were finished
                    time.sleep(0.001)

            # Now run a batch of single threaded jobs
            batch_left = single_queue_size
            while items_left_single > 0 and batch_left > 0:
                idx, item = next(items_single)
                result = func(item)
                results[idx] = result

                # Current job finished
                items_left -= 1
                items_left_single -= 1
                batch_left -= 1
                pbar.update(1)

        pbar.close()
        return results

    # ==============================================================================================

    @staticmethod
    def audio_to_mono_wav(paths: Tuple[str, str], sample_rate: int) -> bool:
        path_source = paths[0]
        path_target = paths[1]

        _, extension = os.path.splitext(os.path.basename(path_source))
        extension = extension.replace(".", "")

        if extension == "opus":
            with open(path_source, "rb") as file:
                opus_audio_bytes = file.read()
            opus_data = BytesIO(opus_audio_bytes)
            try:
                audio = AudioSegment.from_file(opus_data, codec="opus")
            except exceptions.CouldntDecodeError as e:
                print("Error with file:", path_source)
                print("Ignoring:", str(e))
                return False

        else:
            try:
                audio = AudioSegment.from_file(path_source, extension)

            except exceptions.CouldntDecodeError:
                # Solve a problem with gothic1's pcm_s4le encoding
                # See: https://stackoverflow.com/a/67071314
                data, sr = sf.read(path_source)
                tmpfile = "tmp-audio-" + uuid.uuid4().hex + "." + extension
                sf.write(tmpfile, data, sr)
                audio = AudioSegment.from_file(tmpfile, extension)
                os.remove(tmpfile)

        audio = audio.set_frame_rate(sample_rate)
        audio = audio.set_channels(1)
        audio.export(path_target, codec="pcm_s16le", format="wav")

        return True

    # ==============================================================================================

    @staticmethod
    def convert_audio(
        audio_mappings: List[Tuple[str, str]], sample_rate: int
    ) -> List[bool]:
        """Convert list of audio files with input and output paths from various encodings
        into mono-channel wav format with given sample rate."""

        paths = [am[0] for am in audio_mappings]
        with futures.ProcessPoolExecutor(AbstractWriter.get_real_cpu_count()) as p:
            list(
                tqdm.tqdm(
                    p.map(AbstractWriter.check_file_exists, paths), total=len(paths)
                )
            )

        max_size = int(psutil.virtual_memory().available / mp.cpu_count() / 128)
        print("Separating along files larger than {} bytes".format(max_size))
        parallel = [bool(os.path.getsize(p) <= max_size) for p, _ in audio_mappings]

        pfunc = partial(AbstractWriter.audio_to_mono_wav, sample_rate=sample_rate)
        success = AbstractWriter.improved_pool_map(pfunc, audio_mappings, parallel)

        return success

    # ==============================================================================================

    @staticmethod
    def cut_wav_part(
        audio: AudioSegment, start: float, stop: float, path_target: str
    ) -> None:
        """Cut given segment from audio and save it as a new file"""

        start = int(start * 1000)
        stop = int(stop * 1000)
        seg = audio[start:stop]

        if not os.path.isdir(os.path.dirname(path_target)):
            os.makedirs(os.path.dirname(path_target))

        seg.export(path_target, format="wav")

    # ==============================================================================================

    @staticmethod
    def cut_segments(entry: dict, datadir: str) -> List[dict]:
        """Cut all segments from an dataset entry and create new simple entries"""

        if "segments" not in entry:
            return [entry]

        afp = os.path.join(datadir, entry["filepath"])
        audio = AudioSegment.from_file(afp)
        new_entries = []

        for segment in entry["segments"]:
            fp = afp.replace(
                ".wav", "_{}.wav".format(int(segment["time_begin"] * 1000))
            )
            AbstractWriter.cut_wav_part(
                audio, segment["time_begin"], segment["time_end"], fp
            )

            new_entry = copy.deepcopy(entry)
            new_entry.pop("segments")
            new_entry["filepath"] = fp.replace(datadir, "")
            new_entry.update(segment)
            new_entry.pop("time_begin")
            new_entry.pop("time_end")
            new_entries.append(new_entry)

        # Delete old audio file
        os.remove(afp)

        return new_entries

    # ==============================================================================================

    @staticmethod
    def split_segments(dataset: List[dict], datadir: str) -> List[dict]:
        max_size = int(psutil.virtual_memory().available / mp.cpu_count() / 4)
        print("Separating along files larger than {} bytes".format(max_size))
        parallel = [
            bool(os.path.getsize(os.path.join(datadir, e["filepath"])) <= max_size)
            for e in dataset
        ]

        pfunc = partial(AbstractWriter.cut_segments, datadir=datadir)
        dataset = AbstractWriter.improved_pool_map(pfunc, dataset, parallel)
        dataset = list(itertools.chain.from_iterable(dataset))

        return dataset
