import copy
import json
import os
from typing import List

from .. import utils
from .abstract_writer import AbstractWriter

# ==================================================================================================


class Writer(AbstractWriter):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def save_dataset(
        self, dataset: List[dict], path: str, sample_rate: int, overwrite: bool
    ) -> List[dict]:
        if overwrite:
            raise NotImplementedError

        super().save_dataset(dataset, path, sample_rate, overwrite)

        # Add duration key if not already existing
        dataset = utils.maybe_add_duration(dataset)

        partitions = utils.get_partitions(dataset)
        has_partition = len(partitions) > 0

        print("\nWriting annotations ...")
        ds = []
        for entry in dataset:
            e = {
                "audio_filepath": entry["filepath"],
                "duration": entry["duration"],
                "text": entry["text"],
            }
            if has_partition:
                e["partition"] = entry["partition"]
            ds.append(e)

        # Write full dataset
        dsa = copy.deepcopy(ds)
        for e in dsa:
            if has_partition:
                e.pop("partition")
        dsaj = [json.dumps(e) for e in dsa]
        content = "\n".join(dsaj)
        mft_path = os.path.join(path, "nm_all.json")
        with open(mft_path, "w+", encoding="utf-8") as file:
            file.write(content)

        # Write partitions
        if has_partition:
            for part in partitions:
                dsp = [e for e in ds if e["partition"] == part]

                dsp = copy.deepcopy(dsp)
                for e in dsp:
                    e.pop("partition")

                dspj = [json.dumps(e) for e in dsp]
                content = "\n".join(dspj)

                mft_path = os.path.join(path, "nm_" + part + ".json")
                with open(mft_path, "w+", encoding="utf-8") as file:
                    file.write(content)

        return dataset
