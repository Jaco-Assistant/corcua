import os
from typing import List

import pandas as pd

from .. import utils
from .abstract_writer import AbstractWriter

# ==================================================================================================


class Writer(AbstractWriter):
    def __init__(self) -> None:
        pass

    # ==============================================================================================

    def save_dataset(
        self, dataset: List[dict], path: str, sample_rate: int, overwrite: bool
    ) -> List[dict]:
        if overwrite:
            raise NotImplementedError

        super().save_dataset(dataset, path, sample_rate, overwrite)

        partitions = utils.get_partitions(dataset)
        has_partition = len(partitions) > 0

        print("\nWriting annotations ...")
        ds = []
        for entry in dataset:
            e = {
                "transcript": entry["text"].replace(",", " "),
                "wav_filename": entry["filepath"],
                "wav_filesize": os.path.getsize(entry["filepath"]),
            }
            if has_partition:
                e["partition"] = entry["partition"]
            ds.append(e)

        # Write full dataset
        ds_pd = pd.DataFrame(ds)
        csv_path = os.path.join(path, "ds_all.csv")
        if has_partition:
            dsa = ds_pd.drop("partition", axis=1)
        dsa.to_csv(csv_path, index=False, encoding="utf-8", sep=",")

        # Write partitions
        if has_partition:
            for part in partitions:
                dsp = ds_pd[ds_pd["partition"] == part]
                dsp = dsp.drop("partition", axis=1)
                csv_path = os.path.join(path, "ds_" + part + ".csv")
                dsp.to_csv(csv_path, index=False, encoding="utf-8", sep="\t")

        return dataset
