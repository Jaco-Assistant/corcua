from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.link = "https://drive.infomaniak.com/app/share/327683/85ec2469-8a64-4a8a-8de0-43e262d32d38/16/download"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        super().download_dataset(path, overwrite, args)

        AbstractDownloader.download_and_extract_zip(self.link, path)
