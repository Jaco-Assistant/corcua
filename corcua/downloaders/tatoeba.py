import json
import multiprocessing as mp
import os
from functools import partial
from typing import Tuple

import pandas as pd
import tqdm

from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.link_sens = "http://downloads.tatoeba.org/exports/sentences.tar.bz2"
        self.link_swa = (
            "http://downloads.tatoeba.org/exports/sentences_with_audio.tar.bz2"
        )
        self.link_audio = "https://audio.tatoeba.org/sentences/{}/{}.mp3"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["language", "license_filter"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        print("Downloading metadata ...")
        AbstractDownloader.download_and_extract_tarbz2(self.link_sens, path)
        AbstractDownloader.download_and_extract_tarbz2(self.link_swa, path)

        print("Checking downloadable files ...")
        path_sens = os.path.join(path, "sentences.csv")
        sentences = pd.read_csv(
            path_sens,
            encoding="utf-8",
            sep="\t",
            keep_default_na=False,
            names=["id", "language", "text"],
        )
        path_swa = os.path.join(path, "sentences_with_audio.csv")
        sens_wa = pd.read_csv(
            path_swa,
            encoding="utf-8",
            sep="\t",
            keep_default_na=False,
            names=["id", "speaker", "license", "notes"],
        )

        sentences = self.check_downloadable_files(sentences, sens_wa, args["language"])
        if args["license_filter"] != "":
            sentences = sentences[sentences["license"] == args["license_filter"]]

        print("Downloading audio files ...")
        os.makedirs(os.path.join(path, "audio/"), exist_ok=True)
        self.download_files(sentences, path, args["language"])

        # Save sentence data
        jpath = os.path.join(path, "metadata.json")
        with open(jpath, "w+", encoding="utf-8") as file:
            data = sentences.to_dict(orient="records")
            json.dump(data, file, indent=2)

        # Delete the sentence collection files
        os.remove(path_sens)
        os.remove(path_swa)

    # ==============================================================================================

    @staticmethod
    def check_downloadable_files(
        sentences: pd.DataFrame, sentences_with_audio: pd.DataFrame, language: str
    ) -> pd.DataFrame:
        """Only audio files that have a license can be used outside tatoeba"""

        sentences = sentences[sentences["language"] == language]
        print('Found {} sentences in language "{}"'.format(len(sentences), language))

        sentences = sentences.merge(sentences_with_audio, how="inner", on="id")
        print("Of them {} have an audio file".format(len(sentences)))

        sentences = sentences[sentences["license"] != "\\N"]
        print("And {} audio files also have a license".format(len(sentences)))

        return sentences

    # ==============================================================================================

    def download_single(self, item: Tuple[str, str]) -> None:
        self.download(link=item[0], filepath=item[1], show_progress=False)

    # ==============================================================================================

    def download_files(self, sentences: pd.DataFrame, path: str, lang: str) -> None:
        ids = sentences["id"].tolist()
        items = []
        for idx in ids:
            link = self.link_audio.format(lang, idx)
            target_path = os.path.join(path, "audio/{}.mp3".format(idx))
            items.append([link, target_path])

        pfunc = partial(self.download_single)
        with mp.Pool(mp.cpu_count()) as p:
            _ = list(tqdm.tqdm(p.imap(pfunc, items), total=len(items)))
