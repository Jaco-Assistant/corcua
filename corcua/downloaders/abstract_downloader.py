import gzip
import os
import shutil
import tarfile
import zipfile
from abc import ABCMeta, abstractmethod
from urllib import request

from progressist import ProgressBar

# ==================================================================================================


class AbstractDownloader:
    __metaclass__ = ABCMeta

    # ==============================================================================================

    @abstractmethod
    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        """Creates the dataset directory and deletes old data if overwrite flag is set"""

        if os.path.exists(path):
            if overwrite:
                shutil.rmtree(path, ignore_errors=True)
            else:
                msg = "The given directory already exists: {}"
                raise IsADirectoryError(msg.format(path))

        os.makedirs(path)

    # ==============================================================================================

    @staticmethod
    def download_and_extract_targz(
        link: str, target_path: str, show_progress: bool = True
    ) -> None:
        if not os.path.isdir(target_path):
            os.makedirs(target_path)

        path = os.path.join(target_path, "tmp.tar.gz")
        AbstractDownloader.download(link, path, show_progress)

        with tarfile.open(path) as tfile:
            tfile.extractall(path=target_path)
        os.remove(path)

    # ==============================================================================================

    @staticmethod
    def download_and_extract_gz(
        link: str, target_path: str, show_progress: bool = True
    ) -> None:
        target_dir = os.path.dirname(target_path)
        if not os.path.isdir(target_dir):
            os.makedirs(target_dir)

        path = os.path.join(target_dir, "tmp.gz")
        AbstractDownloader.download(link, path, show_progress)

        with open(path, "rb") as gfile:
            with open(target_path, "w", encoding="utf8") as tfile:
                content = gzip.decompress(gfile.read()).decode("utf-8")
                tfile.write(content)
        os.remove(path)

    # ==============================================================================================

    @staticmethod
    def download_and_extract_zip(
        link: str, target_path: str, show_progress: bool = True
    ) -> None:
        if not os.path.isdir(target_path):
            os.makedirs(target_path)

        path = os.path.join(target_path, "tmp.zip")
        AbstractDownloader.download(link, path, show_progress)

        with zipfile.ZipFile(path, "r") as zfile:
            zfile.extractall(path=target_path)
        os.remove(path)

    # ==============================================================================================

    @staticmethod
    def download_and_extract_tarbz2(
        link: str, target_path: str, show_progress: bool = True
    ) -> None:
        if not os.path.isdir(target_path):
            os.makedirs(target_path)

        path = os.path.join(target_path, "tmp.tar.bz2")
        AbstractDownloader.download(link, path, show_progress)

        with tarfile.open(path, "r:bz2") as tfile:
            tfile.extractall(path=target_path)
        os.remove(path)

    # ==============================================================================================

    @staticmethod
    def download(link: str, filepath: str, show_progress: bool = True) -> None:
        # An extra header is required to download the testfiles from gitlab,
        # without it the request gets blocked
        opener = request.build_opener()
        opener.addheaders = [("User-Agent", "XYZ/3.0")]
        request.install_opener(opener)

        if show_progress:
            pbar = ProgressBar(template="Download |{animation}| {done:B}/{total:B}")
            request.urlretrieve(link, filepath, reporthook=pbar.on_urlretrieve)
        else:
            request.urlretrieve(link, filepath)
