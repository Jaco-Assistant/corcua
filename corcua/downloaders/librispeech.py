from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://www.openslr.org/resources/12/"

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["testonly"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        link = self.base_link + "test-clean.tar.gz"
        AbstractDownloader.download_and_extract_targz(link, path)
        link = self.base_link + "test-other.tar.gz"
        AbstractDownloader.download_and_extract_targz(link, path)

        if not args["testonly"]:
            link = self.base_link + "dev-clean.tar.gz"
            AbstractDownloader.download_and_extract_targz(link, path)
            link = self.base_link + "dev-other.tar.gz"
            AbstractDownloader.download_and_extract_targz(link, path)

            link = self.base_link + "train-clean-100.tar.gz"
            AbstractDownloader.download_and_extract_targz(link, path)
            link = self.base_link + "train-clean-360.tar.gz"
            AbstractDownloader.download_and_extract_targz(link, path)
            link = self.base_link + "train-other-500.tar.gz"
            AbstractDownloader.download_and_extract_targz(link, path)
