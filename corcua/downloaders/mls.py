from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://dl.fbaipublicfiles.com/mls/mls_{}_opus.tar.gz"
        self.languages = {
            "de": "german",
            "en": "english",
            "es": "spanish",
            "fr": "french",
            "it": "italian",
            "nl": "dutch",
            "pl": "polish",
            "pt": "portugese",
        }

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        if not args["language"] in self.languages:
            msg = "There is no MLS URL present for language {}!"
            raise ValueError(msg.format(args["language"]))

        link = self.base_link.format(self.languages[args["language"]])
        AbstractDownloader.download_and_extract_targz(link, path)
