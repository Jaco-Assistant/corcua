from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://corpora.uni-hamburg.de/hzsk/de/islandora/object/file:"
        self.languages = {
            "de": "swc-2.0_de-with-audio/datastream/TAR/de-with-audio.tar",
            "en": "swc-2.0_nl-with-audio/datastream/TAR/nl-with-audio.tar",
            "nl": "swc-2.0_nl-with-audio/datastream/TAR/nl-with-audio.tar",
        }

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        if not args["language"] in self.languages:
            msg = "There is no voxforge URL present for language {}!"
            raise ValueError(msg.format(args["language"]))

        link = self.base_link + self.languages[args["language"]]
        AbstractDownloader.download_and_extract_targz(link, path)
