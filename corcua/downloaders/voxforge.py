import re
from urllib import request

import tqdm

from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "http://www.repository.voxforge1.org/downloads/{}/Trunk/Audio/Main/16kHz_16bit/"
        self.languages = [
            "bg",
            "ca",
            "de",
            "el",
            "en",
            "es",
            "fa",
            "fr",
            "he",
            "hr",
            "it",
            "nl",
            "pt",
            "ru",
            "sq",
            "tr",
            "uk",
            "zh",
        ]

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        if not args["language"] in self.languages:
            msg = "There is no voxforge URL present for language {}!"
            raise ValueError(msg.format(args["language"]))

        link = self.base_link.format(args["language"])
        with request.urlopen(link) as url:
            page = url.read().decode("utf-8")

        # Extract file names from the directory path
        pattern = re.compile(r'href="(.*\.tgz)"')
        files = pattern.findall(page)
        flinks = [link + f for f in files]

        print("Downloading {} files ...".format(len(flinks)))
        for flink in tqdm.tqdm(flinks):
            AbstractDownloader.download_and_extract_targz(
                flink, target_path=path, show_progress=False
            )
