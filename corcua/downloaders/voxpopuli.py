"""See: https://github.com/facebookresearch/voxpopuli/blob/main/voxpopuli/download_audios.py"""

import os

from .abstract_downloader import AbstractDownloader

# ==================================================================================================


class Downloader(AbstractDownloader):
    def __init__(self) -> None:
        self.base_link = "https://dl.fbaipublicfiles.com/voxpopuli/"
        self.audio_link = self.base_link + "audios/original_{}.tar"
        self.metadata_link = self.base_link + "annotations/asr/asr_{}.tsv.gz"

        self.languages = [
            "en",
            "de",
            "fr",
            "es",
            "pl",
            "it",
            "ro",
            "hu",
            "cs",
            "nl",
            "fi",
            "hr",
            "sk",
            "sl",
            "et",
            "lt",
        ]
        self.years = list(range(2009, 2020 + 1))

    # ==============================================================================================

    def download_dataset(self, path: str, overwrite: bool, args: dict) -> None:
        req_args = ["language"]
        if not all((r in args for r in req_args)):
            raise AttributeError("Required arguments:", req_args)

        super().download_dataset(path, overwrite, args)

        for year in self.years:
            link = self.audio_link.format(year)
            print("Downloading:", link)
            AbstractDownloader.download_and_extract_targz(link, path)

        print("Downloading metadata ...")
        link = self.metadata_link.format(args["language"])
        tpath = os.path.join(path, "metadata.tsv")
        AbstractDownloader.download_and_extract_gz(link, tpath)
