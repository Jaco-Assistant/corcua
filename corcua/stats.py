from typing import List

import pandas as pd

from . import utils

# ==================================================================================================


def get_duration(dataset: List[dict]) -> List[dict]:
    """Print full dataset duration. Also addes duration keys if required."""

    dataset = utils.maybe_add_duration(dataset)
    length = sum((e["duration"] for e in dataset))
    msg = "Total length of the dataset is {} hours"
    print(msg.format(utils.seconds_to_hours(length)))

    return dataset


# ==================================================================================================


def top_key_durations(dataset: List[dict], key: str, topk: int) -> None:
    """Print top values of the given key by their total duration"""

    if "duration" not in dataset[0]:
        raise ValueError("Add duration key first!")

    if key in ["text", "duration", "filepath"]:
        raise ValueError("Don't try this again")

    if key not in dataset[0]:
        raise ValueError("This key is not existing in this dataset")

    ds_pd = pd.DataFrame(dataset)
    df = ds_pd.groupby(key)["duration"].sum()
    df = df.sort_values(ascending=False)
    print(df.head(topk))
