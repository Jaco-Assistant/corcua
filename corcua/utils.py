import base64
import os
from concurrent.futures import ProcessPoolExecutor as Pool
from functools import partial
from typing import Dict, List, Optional, Tuple

import audioread
import chardet
import pandas as pd
import soundfile as sf
import tqdm

# ==================================================================================================


def get_duration(filename: str) -> float:
    """Get duration of the given audio file"""
    try:
        # Much faster, but does not work with every extension
        length = float(sf.info(filename).duration)
    except RuntimeError:
        # Slow but more extensions available
        with audioread.audio_open(filename) as file:
            length = float(file.duration)
    return length


# ==================================================================================================


def add_duration(entry: dict) -> dict:
    """Add audio duration to dataset item"""

    if "segments" not in entry:
        # Get duration from file
        # Some datasets have relative paths, which might not work from the current path
        if os.path.exists(entry["filepath"]):
            entry["duration"] = get_duration(entry["filepath"])
        else:
            raise ValueError("Can't access the files and add durations automatically")
    else:
        # Get duration from segment lengths
        duration = 0
        for seg in entry["segments"]:
            duration += seg["time_end"] - seg["time_begin"]
        entry["duration"] = duration

    return entry


# ==================================================================================================


def maybe_add_duration(dataset: List[dict]) -> List[dict]:
    """Add duration keys to dataset if not already existing"""

    if not all("duration" in e for e in dataset):
        print("\nCalculating dataset duration ...")

        # If you get an error like this 'OSError: [Errno 22] Invalid argument'
        # try to run the script from command line in case you're running it from your IDE shell
        pfunc = partial(add_duration)
        with Pool(len(os.sched_getaffinity(0))) as p:
            dataset = list(tqdm.tqdm(p.map(pfunc, dataset), total=len(dataset)))

    return dataset


# ==================================================================================================


def get_partitions(dataset: List[dict]) -> List[str]:
    if "partition" in dataset[0]:
        partitions = [e["partition"] for e in dataset]
        partitions = sorted(set(partitions))
    else:
        partitions = []

    return partitions


# ==================================================================================================


def add_partitions(
    dataset: List[dict], parts: List[Tuple[str, float]], group_key: Optional[str] = None
) -> List[dict]:
    """Split dataset into partitions. Optionally split by key like "speaker". If dataset is not
    exactly dividable, the last given partition might be a little smaller."""

    if "partition" in dataset[0]:
        raise ValueError("This dataset already has partitions")

    if not sum((p[1] for p in parts)) == 1:
        raise ValueError("Partition sizes must sum to 1")

    dataset = maybe_add_duration(dataset)

    if group_key is None:
        sorted_ds: List[dict] = sorted(dataset, key=lambda e: e["duration"])
        sorted_ds.reverse()
        smalles_part_idx = 0
        part_lengths = [[i, 0] for i in range(len(parts))]

        # Add next element to (relativ) smallest partition to ensure an almost equal split
        for e in sorted_ds:
            e["partition"] = parts[smalles_part_idx][0]

            # Get new smallest partition
            dur_factor = e["duration"] * (1 / parts[smalles_part_idx][1])
            part_lengths[smalles_part_idx][1] += dur_factor
            smalles_part_idx = sorted(part_lengths, key=lambda e: e[1])[0][0]

    else:
        ds_pd = pd.DataFrame(dataset)
        df = ds_pd.groupby(group_key)["duration"].sum()
        df = df.sort_values(ascending=False)
        group_durs = df.to_dict()

        smalles_part_idx = 0
        part_members: Dict[str, List[str]] = {}
        for p in parts:
            part_members[p[0]] = []
        part_lengths = [[i, 0] for i in range(len(parts))]

        # Similar to approach without keys, but assigning groups instead of entries to a partition
        for gd in group_durs:
            part_members[parts[smalles_part_idx][0]].append(gd)

            dur_factor = group_durs[gd] * (1 / parts[smalles_part_idx][1])
            part_lengths[smalles_part_idx][1] += dur_factor
            smalles_part_idx = sorted(part_lengths, key=lambda e: e[1])[0][0]

        pm_reversed = dict((v, k) for k, vals in part_members.items() for v in vals)
        for e in dataset:
            e["partition"] = pm_reversed[e[group_key]]

    return dataset


# ==================================================================================================


def seconds_to_hours(secs: float) -> str:
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_encoding_type(filepath: str) -> str:
    """Get file encoding type"""

    with open(filepath, "rb") as f:
        rawdata = f.read()

    encoding: str = chardet.detect(rawdata)["encoding"]
    return encoding


# ==================================================================================================


class PathSaveString:
    """Compact string representation in alphanumeric characters.
    Based on https://stackoverflow.com/a/58918224 and https://stackoverflow.com/a/10326291
    """

    def __init__(self) -> None:
        self.chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.base = len(self.chars)
        self.char_map = {k: v for v, k in enumerate(self.chars)}

    def encode(self, text: str) -> str:
        bstr = base64.b16encode(text.encode("utf-8")).decode("utf-8")
        bnum = int(bstr, 16)

        result = ""
        while bnum:
            result = self.chars[bnum % self.base] + result
            bnum //= self.base
        if not result:
            result = self.chars[0]

        return result

    def decode(self, text: str) -> str:
        bnum = 0
        for char in text:
            bnum = bnum * self.base + self.char_map[char]

        bstr = str(hex(bnum))[2:].upper()
        result = base64.b16decode(bstr.encode("utf-8")).decode("utf-8")

        return result
